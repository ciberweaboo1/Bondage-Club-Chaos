let assets = {};

assets.cop = [
    {
        "Name": "H0960",
        "Group": "Height",
        "Color": []
    },
    {
        "Name": "Large",
        "Group": "BodyUpper",
        "Color": "Asian",
        "Property": {
            "Type": null
        }
    },
    {
        "Name": "Blush",
        "Group": "Blush",
        "Color": "Default"
    },
    {
        "Name": "Fluids",
        "Group": "Fluids",
        "Color": "Default"
    },
    {
        "Name": "Emoticon",
        "Group": "Emoticon",
        "Color": "Default"
    },
    {
        "Name": "Default",
        "Group": "Hands",
        "Color": "Default"
    },
    {
        "Name": "XLarge",
        "Group": "BodyLower",
        "Color": "Asian"
    },
    {
        "Name": "HairFront1b",
        "Group": "HairFront",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "HairBack16",
        "Group": "HairBack",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "Eyes5",
        "Group": "Eyes2",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "Eyes5",
        "Group": "Eyes",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "Nipples2",
        "Group": "Nipples",
        "Color": "Default"
    },
    {
        "Name": "SheHer",
        "Group": "Pronouns",
        "Color": "Default"
    },
    {
        "Name": "Default",
        "Group": "Head",
        "Color": "Default"
    },
    {
        "Name": "Bra1",
        "Group": "Bra",
        "Color": "Default"
    },
    {
        "Name": "PoliceWomanHat",
        "Group": "Hat",
        "Color": [
            "Default",
            "#2574DA"
        ]
    },
    {
        "Name": "Eyebrows2",
        "Group": "Eyebrows",
        "Color": [
            "#700F96",
            "Default"
        ],
        "Property": {
            "Type": "s0p0r0",
            "Difficulty": 0,
            "Block": [],
            "Effect": [],
            "Hide": [],
            "HideItem": [],
            "AllowActivity": [],
            "Attribute": []
        }
    },
    {
        "Name": "Full",
        "Group": "Mouth",
        "Color": [
            "#4C087C",
            "#aa5555"
        ],
        "Property": {
            "Type": "l0t0",
            "Difficulty": 0,
            "Block": [],
            "Effect": [],
            "Hide": [],
            "HideItem": [],
            "AllowActivity": [],
            "Attribute": []
        }
    },
    {
        "Name": "Pussy1",
        "Group": "Pussy",
        "Color": "Default"
    },
    {
        "Name": "ShortPencilSkirt",
        "Group": "ClothLower",
        "Color": [
            "#2C5EEC"
        ]
    },
    {
        "Name": "Pantyhose1",
        "Group": "SuitLower",
        "Color": "Default"
    },
    {
        "Name": "Panties1",
        "Group": "Panties",
        "Color": "Default"
    },
    {
        "Name": "CorsetShirt",
        "Group": "Cloth",
        "Color": [
            "#05317B",
            "Default"
        ]
    },
    {
        "Name": "IDCard",
        "Group": "Necklace",
        "Color": [
            "Default",
            "Default"
        ]
    },
    {
        "Name": "Corset6",
        "Group": "Corset",
        "Color": [
            "#435331",
            "#363535",
            "#A08759"
        ]
    },
    {
        "Name": "LatexSocks1",
        "Group": "Socks",
        "Color": [
            "#B3B3B3"
        ]
    },
    {
        "Name": "ThighHighLatexHeels",
        "Group": "Shoes",
        "Color": "Default"
    },
    {
        "Name": "LatexElbowGloves",
        "Group": "Gloves",
        "Color": [
            "#8D8D8D"
        ]
    }
];

assets.prisoner = [
    {
        "Name": "CheerleaderTop",
        "Group": "Cloth",
        "Color": [
            "#FF6000",
            "#FF6000",
            "#FF6000",
            "#FFFFFF",
            "#FF6000"
        ],
        "Property": {
            "Text": "INMATE"
        }
    },
    {
        "Name": "DominatrixLeotard",
        "Group": "Bra",
        "Color": "#FF6000"
    },
    {
        "Name": "Socks5",
        "Group": "Socks",
        "Color": "#FF6000"
    },
    {
        "Name": "H0960",
        "Group": "Height",
        "Color": []
    },
    {
        "Name": "Large",
        "Group": "BodyUpper",
        "Color": "Asian",
        "Property": {
            "Type": null
        }
    },
    {
        "Name": "XLarge",
        "Group": "BodyLower",
        "Color": "Asian"
    },
    {
        "Name": "HairFront1b",
        "Group": "HairFront",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "HairBack16",
        "Group": "HairBack",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "Eyebrows2",
        "Group": "Eyebrows",
        "Color": [
            "#700F96",
            "Default"
        ],
        "Property": {
            "Type": "s0p0r0",
            "Difficulty": 0,
            "Block": [],
            "Effect": [],
            "Hide": [],
            "HideItem": [],
            "AllowActivity": [],
            "Attribute": []
        }
    },
    {
        "Name": "Eyes5",
        "Group": "Eyes",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "Eyes5",
        "Group": "Eyes2",
        "Color": [
            "#6706B4"
        ]
    },
    {
        "Name": "Full",
        "Group": "Mouth",
        "Color": [
            "#4C087C",
            "#aa5555"
        ],
        "Property": {
            "Type": "l0t0",
            "Difficulty": 0,
            "Block": [],
            "Effect": [],
            "Hide": [],
            "HideItem": [],
            "AllowActivity": [],
            "Attribute": []
        }
    },
    {
        "Name": "Nipples2",
        "Group": "Nipples",
        "Color": "Default"
    },
    {
        "Name": "Pussy1",
        "Group": "Pussy",
        "Color": "Default"
    },
    {
        "Name": "SheHer",
        "Group": "Pronouns",
        "Color": "Default"
    },
    {
        "Name": "Default",
        "Group": "Head",
        "Color": "Default"
    },
    {
        "Name": "Blush",
        "Group": "Blush",
        "Color": "Default"
    },
    {
        "Name": "Fluids",
        "Group": "Fluids",
        "Color": "Default"
    },
    {
        "Name": "Emoticon",
        "Group": "Emoticon",
        "Color": "Default"
    },
    {
        "Name": "Default",
        "Group": "Hands",
        "Color": "Default"
    },
    {
        "Name": "LargeDildo",
        "Group": "ItemMouth",
        "Color": [
            "#333333"
        ]
    },
    {
        "Name": "WiffleGag",
        "Group": "ItemMouth2",
        "Color": [
            "Default",
            "#FC9000"
        ],
        "Property": {
            "Type": "Tight"
        }
    },
    {
        "Name": "FuturisticMuzzle",
        "Group": "ItemMouth3",
        "Color": [
            "#FF7E00",
            "#FF7E00",
            "#FF7E00",
            "Default"
        ],
        "Property": {
            "Block": [
                "ItemMouth",
                "ItemMouth2"
            ],
            "Effect": [
                "BlockMouth",
                "Lock"
            ],
            "Hide": [
                "Mouth"
            ],
            "HideItem": [
                "ItemNoseNoseRing"
            ],
            "AllowActivity": [],
            "Attribute": [
                "FuturisticRecolor"
            ],
            "Type": "n1h1s1",
            "Difficulty": 2,
            "LockedBy": "ExclusivePadlock",
            "LockMemberNumber": 133997
        }
    },
    {
        "Name": "WristShackles",
        "Group": "ItemArms",
        "Color": [
            "Default",
            "Default"
        ],
        "Property": {
            "Type": null,
            "Effect": [
                "Lock"
            ],
            "LockedBy": "ExclusivePadlock",
            "LockMemberNumber": 133997
        }
    },
    {
        "Name": "FuturisticMittens",
        "Group": "ItemHands",
        "Color": [
            "#FF7E00",
            "#3B7F2C",
            "#DA7007",
            "Default"
        ],
        "Property": {
            "Difficulty": 8,
            "SelfUnlock": false,
            "Effect": [
                "Block",
                "Prone",
                "MergedFingers",
                "Lock"
            ],
            "Block": [
                "ItemHandheld"
            ],
            "Hide": [
                "ItemHandheld"
            ],
            "Type": null,
            "LockedBy": "ExclusivePadlock",
            "LockMemberNumber": 133997
        }
    },
    {
        "Name": "Irish8Cuffs",
        "Group": "ItemFeet",
        "Color": "Default",
        "Property": {
            "Effect": [
                "Lock"
            ],
            "LockedBy": "ExclusivePadlock",
            "LockMemberNumber": 133997
        }
    },
    {
        "Name": "Cage",
        "Group": "ItemDevices",
        "Color": [
            "Default",
            "Default",
            "Default"
        ],
        "Property": {
            "Effect": [
                "Lock"
            ],
            "MemberNumberListKeys": "133997",
            "LockedBy": "HighSecurityPadlock",
            "LockMemberNumber": 133997
        }
    }
];

export default assets;