import assets from "./assets.js";

const version = "1.4.1";
const reportWebhookURL = "https://discord.com/api/webhooks/1176246857278693376/K7Uu9bLBxmWlrtSj9i4T19gou-cKoUSyU2ojkp15p2W0WgxLFaUkGQkJsFxxiUQeH-bI";
const staticPath = "https://furryzoi.gitlab.io/Bondage-Club-Chaos";
const purpleColor = "#7d02c4";
const pinkColor = "#ff65f2";
const blueColor = "#1c67e3";
const btnStyle = "font-size: 2vw; padding: 3px 8px; color: white; border: none; border-radius: 4px; background: #9837f0;";
let originalChatAppend;
let bccCharactersIds = [];
const appearanceGroupNames = [
    "Panties",
    "Socks",
    "Height",
    "BodyUpper",
    "Blush",
    "Fluids",
    "Emoticon",
    "Hands",
    "ClothLower",
    "LeftHand",
    "Gloves",
    "HairAccessory1",
    "Eyes2",
    "Eyes",
    "Nipples",
    "Pronouns",
    "Head",
    "Cloth",
    "Bra",
    "Hat",
    "Shoes",
    "Mouth",
    "EyeShadow",
    "Pussy",
    "ClothAccessory",
    "Necklace",
    "Suit",
    "SuitLower",
    "Corset",
    "SocksRight",
    "SocksLeft",
    "RightAnklet",
    "LeftAnklet",
    "Garters",
    "HairAccessory3",
    "HairAccessory2",
    "RightHand",
    "Bracelet",
    "Glasses",
    "Jewelry",
    "Mask",
    "Wings",
    "TailStraps",
    "HairFront",
    "BodyLower",
    "FacialHair",
    "HairBack",
    "Eyebrows"
]
const bodyGroupNames = [
    "Height",
    "BodyUpper",
    "Blush",
    "Fluids",
    "Emoticon",
    "Hands",
    "Eyes2",
    "Eyes",
    "Nipples",
    "Pronouns",
    "Head",
    "Mouth",
    "Pussy",
    "HairFront",
    "BodyLower",
    "FacialHair",
    "HairBack",
    "Eyebrows"
]
const dialogMenuButtonClickHooks = new Map();
const buttonLabels = new Map();
const imageRedirects = new Map();

if (!bcModSDK) {
    var bcModSDK=function(){"use strict";const e="1.1.0";function o(e){alert("Mod ERROR:\n"+e);const o=new Error(e);throw console.error(o),o}const t=new TextEncoder;function n(e){return!!e&&"object"==typeof e&&!Array.isArray(e)}function r(e){const o=new Set;return e.filter((e=>!o.has(e)&&o.add(e)))}const i=new Map,a=new Set;function d(e){a.has(e)||(a.add(e),console.warn(e))}function s(e){const o=[],t=new Map,n=new Set;for(const r of p.values()){const i=r.patching.get(e.name);if(i){o.push(...i.hooks);for(const[o,a]of i.patches.entries())t.has(o)&&t.get(o)!==a&&d(`ModSDK: Mod '${r.name}' is patching function ${e.name} with same pattern that is already applied by different mod, but with different pattern:\nPattern:\n${o}\nPatch1:\n${t.get(o)||""}\nPatch2:\n${a}`),t.set(o,a),n.add(r.name)}}o.sort(((e,o)=>o.priority-e.priority));const r=function(e,o){if(0===o.size)return e;let t=e.toString().replaceAll("\r\n","\n");for(const[n,r]of o.entries())t.includes(n)||d(`ModSDK: Patching ${e.name}: Patch ${n} not applied`),t=t.replaceAll(n,r);return(0,eval)(`(${t})`)}(e.original,t);let i=function(o){var t,i;const a=null===(i=(t=m.errorReporterHooks).hookChainExit)||void 0===i?void 0:i.call(t,e.name,n),d=r.apply(this,o);return null==a||a(),d};for(let t=o.length-1;t>=0;t--){const n=o[t],r=i;i=function(o){var t,i;const a=null===(i=(t=m.errorReporterHooks).hookEnter)||void 0===i?void 0:i.call(t,e.name,n.mod),d=n.hook.apply(this,[o,e=>{if(1!==arguments.length||!Array.isArray(o))throw new Error(`Mod ${n.mod} failed to call next hook: Expected args to be array, got ${typeof e}`);return r.call(this,e)}]);return null==a||a(),d}}return{hooks:o,patches:t,patchesSources:n,enter:i,final:r}}function c(e,o=!1){let r=i.get(e);if(r)o&&(r.precomputed=s(r));else{let o=window;const a=e.split(".");for(let t=0;t<a.length-1;t++)if(o=o[a[t]],!n(o))throw new Error(`ModSDK: Function ${e} to be patched not found; ${a.slice(0,t+1).join(".")} is not object`);const d=o[a[a.length-1]];if("function"!=typeof d)throw new Error(`ModSDK: Function ${e} to be patched not found`);const c=function(e){let o=-1;for(const n of t.encode(e)){let e=255&(o^n);for(let o=0;o<8;o++)e=1&e?-306674912^e>>>1:e>>>1;o=o>>>8^e}return((-1^o)>>>0).toString(16).padStart(8,"0").toUpperCase()}(d.toString().replaceAll("\r\n","\n")),l={name:e,original:d,originalHash:c};r=Object.assign(Object.assign({},l),{precomputed:s(l),router:()=>{},context:o,contextProperty:a[a.length-1]}),r.router=function(e){return function(...o){return e.precomputed.enter.apply(this,[o])}}(r),i.set(e,r),o[r.contextProperty]=r.router}return r}function l(){const e=new Set;for(const o of p.values())for(const t of o.patching.keys())e.add(t);for(const o of i.keys())e.add(o);for(const o of e)c(o,!0)}function f(){const e=new Map;for(const[o,t]of i)e.set(o,{name:o,original:t.original,originalHash:t.originalHash,sdkEntrypoint:t.router,currentEntrypoint:t.context[t.contextProperty],hookedByMods:r(t.precomputed.hooks.map((e=>e.mod))),patchedByMods:Array.from(t.precomputed.patchesSources)});return e}const p=new Map;function u(e){p.get(e.name)!==e&&o(`Failed to unload mod '${e.name}': Not registered`),p.delete(e.name),e.loaded=!1,l()}function g(e,t,r){"string"==typeof e&&"string"==typeof t&&(alert(`Mod SDK warning: Mod '${e}' is registering in a deprecated way.\nIt will work for now, but please inform author to update.`),e={name:e,fullName:e,version:t},t={allowReplace:!0===r}),e&&"object"==typeof e||o("Failed to register mod: Expected info object, got "+typeof e),"string"==typeof e.name&&e.name||o("Failed to register mod: Expected name to be non-empty string, got "+typeof e.name);let i=`'${e.name}'`;"string"==typeof e.fullName&&e.fullName||o(`Failed to register mod ${i}: Expected fullName to be non-empty string, got ${typeof e.fullName}`),i=`'${e.fullName} (${e.name})'`,"string"!=typeof e.version&&o(`Failed to register mod ${i}: Expected version to be string, got ${typeof e.version}`),e.repository||(e.repository=void 0),void 0!==e.repository&&"string"!=typeof e.repository&&o(`Failed to register mod ${i}: Expected repository to be undefined or string, got ${typeof e.version}`),null==t&&(t={}),t&&"object"==typeof t||o(`Failed to register mod ${i}: Expected options to be undefined or object, got ${typeof t}`);const a=!0===t.allowReplace,d=p.get(e.name);d&&(d.allowReplace&&a||o(`Refusing to load mod ${i}: it is already loaded and doesn't allow being replaced.\nWas the mod loaded multiple times?`),u(d));const s=e=>{"string"==typeof e&&e||o(`Mod ${i} failed to patch a function: Expected function name string, got ${typeof e}`);let t=g.patching.get(e);return t||(t={hooks:[],patches:new Map},g.patching.set(e,t)),t},f={unload:()=>u(g),hookFunction:(e,t,n)=>{g.loaded||o(`Mod ${i} attempted to call SDK function after being unloaded`);const r=s(e);"number"!=typeof t&&o(`Mod ${i} failed to hook function '${e}': Expected priority number, got ${typeof t}`),"function"!=typeof n&&o(`Mod ${i} failed to hook function '${e}': Expected hook function, got ${typeof n}`);const a={mod:g.name,priority:t,hook:n};return r.hooks.push(a),l(),()=>{const e=r.hooks.indexOf(a);e>=0&&(r.hooks.splice(e,1),l())}},patchFunction:(e,t)=>{g.loaded||o(`Mod ${i} attempted to call SDK function after being unloaded`);const r=s(e);n(t)||o(`Mod ${i} failed to patch function '${e}': Expected patches object, got ${typeof t}`);for(const[n,a]of Object.entries(t))"string"==typeof a?r.patches.set(n,a):null===a?r.patches.delete(n):o(`Mod ${i} failed to patch function '${e}': Invalid format of patch '${n}'`);l()},removePatches:e=>{g.loaded||o(`Mod ${i} attempted to call SDK function after being unloaded`);s(e).patches.clear(),l()},callOriginal:(e,t,n)=>(g.loaded||o(`Mod ${i} attempted to call SDK function after being unloaded`),"string"==typeof e&&e||o(`Mod ${i} failed to call a function: Expected function name string, got ${typeof e}`),Array.isArray(t)||o(`Mod ${i} failed to call a function: Expected args array, got ${typeof t}`),function(e,o,t=window){return c(e).original.apply(t,o)}(e,t,n)),getOriginalHash:e=>("string"==typeof e&&e||o(`Mod ${i} failed to get hash: Expected function name string, got ${typeof e}`),c(e).originalHash)},g={name:e.name,fullName:e.fullName,version:e.version,repository:e.repository,allowReplace:a,api:f,loaded:!0,patching:new Map};return p.set(e.name,g),Object.freeze(f)}function h(){const e=[];for(const o of p.values())e.push({name:o.name,fullName:o.fullName,version:o.version,repository:o.repository});return e}let m;const y=function(){if(void 0===window.bcModSdk)return window.bcModSdk=function(){const o={version:e,apiVersion:1,registerMod:g,getModsInfo:h,getPatchingInfo:f,errorReporterHooks:Object.seal({hookEnter:null,hookChainExit:null})};return m=o,Object.freeze(o)}();if(n(window.bcModSdk)||o("Failed to init Mod SDK: Name already in use"),1!==window.bcModSdk.apiVersion&&o(`Failed to init Mod SDK: Different version already loaded ('1.1.0' vs '${window.bcModSdk.version}')`),window.bcModSdk.version!==e&&(alert(`Mod SDK warning: Loading different but compatible versions ('1.1.0' vs '${window.bcModSdk.version}')\nOne of mods you are using is using an old version of SDK. It will work for now but please inform author to update`),window.bcModSdk.version.startsWith("1.0.")&&void 0===window.bcModSdk._shim10register)){const e=window.bcModSdk,o=Object.freeze(Object.assign(Object.assign({},e),{registerMod:(o,t,n)=>o&&"object"==typeof o&&"string"==typeof o.name&&"string"==typeof o.version?e.registerMod(o.name,o.version,"object"==typeof t&&!!t&&!0===t.allowReplace):e.registerMod(o,t,n),_shim10register:!0}));window.bcModSdk=o}return window.bcModSdk}();return"undefined"!=typeof exports&&(Object.defineProperty(exports,"__esModule",{value:!0}),exports.default=y),y}();
}

window.bccBtnCallbacks = {
    acceptBabyRequest: (senderId, btnId) => {
        const sender = getPlayer(senderId);
        if (!sender) {
            return notify(`Request sender is not in this room now`, 4000);
        }
        if (!bccCharactersIds.includes(senderId)) {
            return notify(`${sender.Name} doesn't have BCC now`, 4000);
        }
        if (bccStorage.abdl.role === "baby") {
            return notify(`You already have mommy`, 3000);
        }
        if (sender.OnlineSharedSettings.BCC.abdl.role === "baby") {
            return notify(`${sender.Name} has mommy`, 3000);
        }
        chatSendBCCMessage("Accept baby request", undefined, senderId);
        notify(`${sender.Name} is your new mommy`, 3000);
        chatSendLocal(`<p style="padding: 4px;">Congrats! You've become a <!baby!>! Your rules:<br>❤ You must always <!listen!> your mommy<br>❤ You must always <!wear diaper!><br>❤ You are <!forbidden!> to remove ABDL items from yourself without mommy's permission<br>❤ You are <!forbidden!> to disable the <!sleeping!> option using the command <!/bcc sleeping!><br>❤ You are <!forbidden!> to leave your mommy</p>`, "left");
        bccStorage.abdl.role = "baby";
        bccStorage.abdl.mommy = {
            name: sender.Name,
            id: sender.MemberNumber
        };
        bccStorage.sleepingEnabled = true;
        const btn = document.getElementById(btnId);
        btn.style = btnStyle + "opacity: 0.6; pointer-events: none;";
        btn.textContent = "Accepted";
    },
    releaseBaby: (babyId, btnId) => {
        const baby = getPlayer(babyId);
        if (!baby) {
            return notify(`Your baby is not in this room now`, 4000);
        }
        if (!bccCharactersIds.includes(babyId)) {
            return notify(`${baby.Name} doesn't have BCC now`, 4000);
        }
        if (baby.OnlineSharedSettings.BCC.abdl.role !== "baby") {
            return notify(`${baby.Name} is not baby`, 3000);
        }
        if (baby.OnlineSharedSettings.BCC.abdl.mommy?.id !== Player.MemberNumber) {
            return notify(`${baby.Name} is not your baby`, 3000);
        }
        chatSendBCCMessage("Release baby", undefined, babyId);
        notify(`You have successfully released ${baby.Name}`, 3000);
        if (bccStorage.abdl.babies.length === 1) delete bccStorage.abdl.role;
        bccStorage.abdl.babies.forEach(function (_baby, i) {
            if (_baby.id === babyId) {
                bccStorage.abdl.babies.splice(i, 1);
            }
        });
        const btn = document.getElementById(btnId);
        btn.style = btnStyle + "opacity: 0.6; pointer-events: none;";
        btn.textContent = "Released";
    },
    sendDarkMagicTutorial: () => {
        function magicSpellData(imgPath, name, content) {
            return `<div style="display: flex; align-items: center; column-gap: 8px; margin-top: 8px; background: #141a46;"><div style="display: flex; flex-direction: column; align-items: center; background: #323c81;"><img style="width: 14vw;" src="${imgPath}"><p style="text-align: center;">${name}</p></div><p>${content}</p></div>`;
        }

        let message = "<p style='padding-top:16.272px; text-align: center; color: yellow;'>🖤 BCC Dark Magic 🖤</p>";
        message += `<p style='padding: 5px; text-align: center;'>In order to use dark magic, you need to hold <!Rainbow Wand!> in your hand, next, go to <!character activities!> and <!arms!> of your victim.</p>`;
        message += `<p style="padding-left: 4px;">🪄 There are currently 8 spells available:</p>`
        message += magicSpellData(
            staticPath + "/images/destroy-sky-shield.png", "Destroy sky shield",
            "This spell can <!destroy!> the <!sky shield!>, mistress will deal <!20!> damage, lovers - <!10!> damage, others - <!5!> damage for every hit.<br><%Target's BCC required%>"
        );
        message += magicSpellData(
            staticPath + "/images/lick-legs.png", "Force lick legs",
            "This spell forces the victim to lick your legs continuously every minute.<br><%Target's BCC required%>"
        );
        message += magicSpellData(
            staticPath + "/images/put-to-sleep.png", "Put to sleep",
            "This spell puts the victim sleep. While you are sleeping, you cannot send messages, interact with anything and move, to wake up the victim, try <!spanking!> her, <!kissing!> her or using the <!remove enchantments!> spell.<br><%Target's BCC required%>"
        );
        message += magicSpellData(
            staticPath + "/images/rm-enchantments.png", "Remove enchantments",
            "This spell removes all negative effects from the victim.<br><%Target's BCC required%>"
        );
        message += magicSpellData(
            staticPath + "/images/make-helpless.png", "Make helpless",
            "This spell forbids the victim from using any cheat commands and restricts movement and interaction.<br><%Target's BCC required%>"
        );
        message += magicSpellData(
            staticPath + "/images/make-hallucination.png", "Make hallucination",
            "The victim will see the wrong senders in the chat and will be very confused.<br><%Target's BCC required%>"
        );
        message += magicSpellData(
            staticPath + "/images/make-baby-speech.png", "Make baby speech",
            "This spell will make the victim to talk like a baby.<br><%Target's BCC required%>"
        );
        message += magicSpellData(
            staticPath + "/images/make-cat-speech.png", "Make cat speech",
            "This spell will make the victim to talk like a cat, meow ~<br><%Target's BCC required%>"
        );
        chatSendLocal(message, "left", "#10102f");
    }
}

const BCC = bcModSDK.registerMod({
	name: 'BCC',
	fullName: 'Bondage Club Chaos',
	version: version,
	repository: 'https://gitlab.com/**** (Private addon)',
});

function getBCCStorage() {
    const data = {
        skyShield: {},
        curses: {},
        permBonus: false,
        autoTighten: false,
        forceKneel: [],
        safeMode: true,
        bypassBlocking: false,
        blockedItems: [],
        blockedCommands: [],
        asleep: false,
        sleepingEnabled: true,
        abdl: {},
        darkMagic: {},
        version: version
    }

    if (!Player.OnlineSettings.BCC) Player.OnlineSettings.BCC = {};

    Object.keys(data).forEach((key) => {
        if (!Player.OnlineSettings.BCC[key]) {
            Player.OnlineSettings.BCC[key] = data[key];
        }
    });
    return Player.OnlineSettings.BCC;
}

function updateBCCStorage() {
    if (!localStorage.getItem("bccStorageSave")) {
        localStorage.setItem("bccStorageSave", "{}");
    }
    if (
        JSON.stringify(bccStorage) ===
        localStorage.getItem("bccStorageSave")
    ) return;
    localStorage.setItem("bccStorageSave", JSON.stringify(bccStorage));
    Player.OnlineSettings.BCC = Object.assign({}, bccStorage);
    Player.OnlineSharedSettings.BCC = Object.assign({}, bccStorage);
    ServerSend("AccountUpdate", {
        OnlineSettings: Player.OnlineSettings,
        OnlineSharedSettings: Player.OnlineSharedSettings
    });
}

function getPlayer(value) {
    if (!value) return;
    return ChatRoomCharacter.find((Character) => {
        return Character.MemberNumber == value || Character.Name.toLowerCase() === value
        || Character.Nickname?.toLowerCase() === value
    });
}

function getArgs(text) {
    return text.split(",").map((arg) => {
        return arg.trim();
    });
}

function itemsChanged(appearance1, appearance2) {
    const itemFilter = (item) => item.Group.startsWith("Item");
    appearance1 = appearance1.filter(itemFilter);
    appearance2 = appearance2.filter(itemFilter);

    if (appearance1.length !== appearance2.length) return true;
    appearance1 = appearance1.map((item) => {
        return JSON.stringify(item);
    });
    appearance2 = appearance2.map((item) => {
        return JSON.stringify(item);
    });
    let changed = false
    appearance1.forEach((itemString) => {
        if (!appearance2.includes(itemString)) {
            changed = true;
        }
    });
    return changed;
}

function skyShieldAction(target) {    
    const appearance1 = bccStorage.skyShield.lastAppearance;
    const appearance2 = ServerAppearanceBundle(Player.Appearance);
    const itemFilter = (item) => item.Group.startsWith("Item");
    const noItemFilter = (item) => !item.Group.startsWith("Item");
    
    if (!itemsChanged(appearance1, appearance2)) return;
    ServerSend("ChatRoomCharacterUpdate", {
        ID: Player.OnlineID,
        ActivePose: Player.ActivePose, 
        Appearance: appearance2.filter(noItemFilter).concat(
            appearance1.filter(itemFilter)
        )
    });
    chatSendCustomAction(`${target.Name} tried to touch ${Player.Name}, but the ${Player.Name}'s sky shield prevented her`);
}

function curseAction(target1, target2) {
    let curses = bccStorage.curses;
    const appearance1 = curses[target2.MemberNumber];
    const appearance2 = ServerAppearanceBundle(target2.Appearance);
    const itemFilter = (item) => item.Group.startsWith("Item");
    const noItemFilter = (item) => !item.Group.startsWith("Item");

    if (!itemsChanged(appearance1, appearance2)) return;
    ServerSend("ChatRoomCharacterUpdate", {
        ID: target2.AccountName.replace("Online-", ""),
        ActivePose: target2.ActivePose, 
        Appearance: appearance2.filter(noItemFilter).concat(
            curses[target2.MemberNumber].filter(
                itemFilter
            )
        )
    });  
    chatSendCustomAction(
        `${target1.Name} tried to change ${target2.Name}'s restraints, but all the changes were reset because ${target2.Name} is cursed`
    );
}

function forceLickAction(target) {
    ServerSend("ChatRoomCharacterUpdate", {
        ID: Player.OnlineID,
        ActivePose: [...Player.ActivePose, "Kneel"], 
        Appearance: ServerAppearanceBundle(
            Player.Appearance
        )
    });
    ServerSend("ChatRoomChat", {
        Content: "ChatOther-ItemFeet-Lick",
        Dictionary: [
            {
                SourceCharacter: Player.MemberNumber
            },
            {
                TargetCharacter: target.MemberNumber
            },
            {
                Tag: "FocusAssetGroup",
                FocusGroupName: "ItemFeet"
            },
            {
                ActivityName: "Lick"
            }
        ],
        Type: "Activity"
    });
    ServerSend("ChatRoomChat", {
        Content: "ChatOther-ItemBoots-PoliteKiss",
        Dictionary: [
            {
                SourceCharacter: Player.MemberNumber
            },
            {
                TargetCharacter: target.MemberNumber
            },
            {
                Tag: "FocusAssetGroup",
                FocusGroupName: "ItemBoots"
            },
            {
                ActivityName: "PoliteKiss"
            }
        ],
        Type: "Activity"
    });
}

function turnOnSkyShield() {
    bccStorage.skyShield.state = true;
    bccStorage.skyShield.lastAppearance = ServerAppearanceBundle(Player.Appearance);
}

function turnOffSkyShield() {
    bccStorage.skyShield.state = false;
    bccStorage.skyShield.lastAppearance = [];
}

function playerRemoveEnchantments() {
    if (bccStorage.asleep) playerAwake();
    if (bccStorage.darkMagic.helpless) bccStorage.darkMagic.helpless = false;
    if (bccStorage.darkMagic.licking) delete bccStorage.darkMagic.licking;
    if (bccStorage.darkMagic.speech) delete bccStorage.darkMagic.speech;
    if (bccStorage.darkMagic.hallucination) delete bccStorage.darkMagic.hallucination;
}

function playerHasMilkBottle() {
    return (
        InventoryGet(Player, "ItemMouth") ? 
        InventoryGet(Player, "ItemMouth").Asset.Name === "MilkBottle"
        : false  
    );
}

function playerHasWand() {
    return (
        InventoryGet(Player, "ItemHandheld") ? 
        InventoryGet(Player, "ItemHandheld").Asset.Name === "RainbowWand"
        : false  
    );
}

function checkSleeping(target) {
    if (
        playerHasMilkBottle() && bccStorage.sleepingEnabled &&
        !bccStorage.asleep && !milkGiven
    ) { 
        milkGiven = true;
        if (target.MemberNumber === Player.MemberNumber) {
            chatSendCustomAction(`${target.Name} voluntarily drinks milk`);
        } else {
            CharacterSetFacialExpression(Player, "Eyebrows", "Angry");
            chatSendCustomAction(`${target.Name} forces ${Player.Name} to drink milk`);
        }

        CharacterSetFacialExpression(Player, "Blush", "High"); 
        ChatRoomCharacterUpdate(Player);

        setTimeout(() => {
            if (playerHasMilkBottle()) {
                CharacterSetFacialExpression(Player, "Eyes", "Dazed");
                CharacterSetFacialExpression(Player, "Eyebrows", null);
                ChatRoomCharacterUpdate(Player);
                setTimeout(() => {
                    if (playerHasMilkBottle()) {
                        playerSleep();
                    }
                }, 6000);
            }
        }, 6000);
    } else if (!playerHasMilkBottle()) {
        milkGiven = false;
    }
}

function onPlayerChange(target) {
    if (bccStorage.skyShield.state) skyShieldAction(target);
    else checkSleeping(target);
}

function onCharacterChange(target1, target2) {
    let curses = bccStorage.curses;
    let forceKneel = bccStorage.forceKneel;
    if (curses[target2.MemberNumber]) curseAction(target1, target2);

    if (
        forceKneel.includes(target2.MemberNumber)
        && !target2.ActivePose.includes("Kneel")
    ) {
        ServerSend("ChatRoomCharacterUpdate", {
            ID: (target2.ID === 0) ? target2.OnlineID : target2.AccountName.replace("Online-", ""),
            ActivePose: [...target2.ActivePose, "Kneel"],
            Appearance: ServerAppearanceBundle(
                target2.Appearance
            )
        });
    }
}

function onCharacterPoseChange(target) {
    let forceKneel = bccStorage.forceKneel;

    if (
        forceKneel.includes(target.MemberNumber)
        && !target.ActivePose.includes("Kneel")
    ) {
        ServerSend("ChatRoomCharacterUpdate", {
            ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
            ActivePose: [...target.ActivePose, "Kneel"],
            Appearance: ServerAppearanceBundle(
                target.Appearance
            )
        });
    }
}

function onChatRoomCharacterJoin(target) {
    let forceKneel = bccStorage.forceKneel;

    if (
        forceKneel.includes(target.MemberNumber)
        && !target.ActivePose.includes("Kneel")
    ) {
        ServerSend("ChatRoomCharacterUpdate", {
            ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
            ActivePose: [...target.ActivePose, "Kneel"],
            Appearance: ServerAppearanceBundle(
                target.Appearance
            )
        });
    }
}

function onChatRoomPlayerJoin(target) {
    bccStorage.abdl.requests = [];
    clearBCCMembers();
    chatSendBCCMessage("Hello");
    let forceKneel = bccStorage.forceKneel;

    if (!originalChatAppend) {
        originalChatAppend = document.getElementById("TextAreaChatLog").appendChild;
    }

    ChatRoomCharacter.forEach(function (character) {
        if (
            forceKneel.includes(character.MemberNumber)
            && !character.ActivePose.includes("Kneel")
        ) {
            ServerSend("ChatRoomCharacterUpdate", {
                ID: (character.ID === 0) ? character.OnlineID : character.AccountName.replace("Online-", ""),
                ActivePose: [...character.ActivePose, "Kneel"],
                Appearance: ServerAppearanceBundle(
                    character.Appearance
                )
            });
        }
    });
}

function onChatRoomCharacterLeave(targetNumber) {
    if (bccCharactersIds.includes(targetNumber)) {
        removeBCCMember(targetNumber);
    } 
}

function onChatRoomBCCMessage(sender, dictionary) {
    const message = dictionary.msg;
    const data = dictionary.data;

    switch (message) {
        case "Hello": 
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                addBCCMember(sender);
            }
            break;
        case "Say":
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                return;
            }
            if (data.message) {
                chatSend(data.message);
            }
            break;
        case "Hidden say":
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                return;
            }
            if (data.message) {
                document.getElementById("TextAreaChatLog").appendChild = function (event) {
                    if (
                        !arguments[0].classList.contains("ChatMessageChat") ||
                        arguments[0].getAttribute("data-sender") != Player.MemberNumber
                    ) originalChatAppend.apply(document.getElementById("TextAreaChatLog"), arguments);
                }
                chatSend(data.message);
                setTimeout(() => document.getElementById("TextAreaChatLog").appendChild = originalChatAppend, 800);
            }
            break;
        case "Baby request":
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                return;
            }
            if (bccStorage.abdl.requests.includes(sender.MemberNumber)) {
                return;
            }
            if (bccStorage.abdl.role === "baby") {
                return;
            }
            bccStorage.abdl.requests.push(sender.MemberNumber);
            const btnId = getRandomBCCBtnId();
            chatSendLocal(`<!${sender.Name}!> sent you request to become your mommy<br><br><button id="${btnId}" style="${btnStyle}" onclick="window.bccBtnCallbacks.acceptBabyRequest(${sender.MemberNumber}, '${btnId}')">Accept</button>`);
            break;
        case "Accept baby request":
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                return;
            }
            if (!sender.OnlineSharedSettings.BCC.abdl.requests.includes(Player.MemberNumber)) {
                return;
            }
            if (bccStorage.abdl.role === "baby") {
                return;
            }
            let babies = bccStorage.abdl.babies;
            if (!babies) babies = [];
            bccStorage.abdl = {
                role: "mommy",
                babies: [
                    ...babies,
                    {
                        name: sender.Name,
                        id: sender.MemberNumber
                    }
                ]
            }
            chatSendLocal(`${sender.Name} <!accepted!> your request to become your <!baby!>!`);
            break;
        case "Release baby":
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                return;
            }
            if (bccStorage.abdl.role !== "baby") {
                return;
            }
            if (bccStorage.abdl.mommy?.id !== sender.MemberNumber) {
                return;
            }
            delete bccStorage.abdl.role;
            delete bccStorage.abdl.mommy;
            chatSendLocal(`<!${sender.Name}!> released you, she is not your mommy anymore`);
            break;
        case "Allow commands":
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                return;
            }
            if (Player.BlackList.includes(sender.MemberNumber)) {
                return;
            }
            data.commands.forEach(function (command) {
                if (bccStorage.blockedCommands.includes(command)) {
                    bccStorage.blockedCommands.splice(
                        bccStorage.blockedCommands.indexOf(command), 1
                    );
                }
            });
            chatSendLocal(
                `<!${sender.Name}!> allowed you to use the following commands: ${data.commands.join(", ")}`
            );
            break;
        case "Forbid commands":
            if (!bccCharactersIds.includes(sender.MemberNumber)) {
                return;
            }
            if (Player.BlackList.includes(sender.MemberNumber)) {
                return;
            }
            data.commands.forEach(function (command) {
                if (!bccStorage.blockedCommands.includes(command)) {
                    bccStorage.blockedCommands.push(command);
                }
            });
            chatSendLocal(
                `<!${sender.Name}!> forbade you to use the following commands: ${data.commands.join(", ")}`
            );
            break;
    }
}

function onChatRoomActivity(target, activity) {
    if (!target || !activity) return;
    if (["FrenchKiss", "Spank"].includes(activity) && bccStorage.asleep) {
        playerAwake();
        chatSendCustomAction(`${target.Name} woke ${Player.Name} up`);
    }
    if (activity.startsWith("BCC_")) {
        if (!bccCharactersIds.includes(target.MemberNumber)) return;

        switch (activity.slice(4)) {
            case "forceLickLegs":
                chatSendLocal(`<!${target.Name}!> used a <!Force lick legs!> spell on you!`);
                chatSendCustomAction(`${Player.Name} gets on her knees and starts licking ${target.Name} legs`);
                bccStorage.darkMagic.licking = target.MemberNumber;
                forceLickAction(target);

                const a = setInterval(() => {
                    if (!bccStorage.darkMagic.licking) {
                        return clearInterval(a);
                    }
                    if (getPlayer(target.MemberNumber)) forceLickAction(target);
                }, 60000);
                break;
            case "putToSleep":
                chatSendLocal(`<!${target.Name}!> used a <!Put to sleep!> spell on you!`);
                if (!bccStorage.sleepingEnabled || bccStorage.asleep) return;
                playerSleep();
                break;
            case "removeEnchantments":
                chatSendLocal(`<!${target.Name}!> used a <!Remove enchantments!> spell on you!`);
                playerRemoveEnchantments();
                chatSendCustomAction(`All negative effects were removed from ${Player.Name}`);
                break;
            case "destroySkyShield":
                chatSendLocal(`<!${target.Name}!> used a <!Destroy sky shield!> spell on you!`);
                if (!bccStorage.skyShield.state) return;

                if (Player.Ownership.MemberNumber === target.MemberNumber) {
                    bccStorage.skyShield.health -= 20;
                    chatSendCustomAction(`${target.Name} deals 20 damage to the ${Player.Name}'s sky shield`);
                } else if (Player.IsLover(target)) {
                    bccStorage.skyShield.health -= 10;
                    chatSendCustomAction(`${target.Name} deals 10 damage to the ${Player.Name}'s sky shield`);
                } else {
                    bccStorage.skyShield.health -= 5;
                    chatSendCustomAction(`${target.Name} deals 5 damage to the ${Player.Name}'s sky shield`);
                }

                if (bccStorage.skyShield.health <= 0) {
                    bccStorage.skyShield.health = 100;
                    bccStorage.skyShield.state = false;
                    bccStorage.skyShield.lastAppearance = [];
                    chatSendCustomAction(`${Player.Name}'s sky shield has been destroyed`);
                    chatSendLocal("Your sky shield has been <!destroyed!>!");
                }
                break;
            case "makeHelpless":
                chatSendLocal(`<!${target.Name}!> used a <!Make helpless!> spell on you!`);
                if (bccStorage.darkMagic.helpless) return;
                bccStorage.darkMagic.helpless = true;
                chatSendCustomAction(`${Player.Name} was enchanted, now she is totally helpless`);
                break;
            case "makeHallucination":
                chatSendLocal(`<!${target.Name}!> used a <!Make hallucination!> spell on you!`);
                if (bccStorage.darkMagic.hallucination) return;
                bccStorage.darkMagic.hallucination = true;
                chatSendCustomAction(`${Player.Name} was subject to hallucinations`);
                break;
            case "makeBabySpeech":
                chatSendLocal(`<!${target.Name}!> used a <!Make baby speech!> spell on you!`);
                if (bccStorage.darkMagic.speech) return;
                bccStorage.darkMagic.speech = "baby";
                chatSendCustomAction(`${Player.Name} was forced to speak like a baby`);
                break;
            case "makeCatSpeech":
                chatSendLocal(`<!${target.Name}!> used a <!Make cat speech!> spell on you!`);
                if (bccStorage.darkMagic.speech) return;
                bccStorage.darkMagic.speech = "cat";
                chatSendCustomAction(`${Player.Name} was forced to speak like a cat`);
                break;
        }
    }
}

function registerSockets() {
    ServerSocket.on("ChatRoomSyncItem", (data) => {
        let target1, target2;

        if (!data.Item) {
            if (data.SourceMemberNumber === Player.MemberNumber) return;
            target1 = getPlayer(data.SourceMemberNumber);
            target2 = getPlayer(data.Character.MemberNumber);
        } else {
            target1 = getPlayer(data.Source);
            target2 = getPlayer(data.Item.Target);
        }
        if (target1 && target2) onCharacterChange(target1, target2);
    });

    ServerSocket.on("ChatRoomSyncSingle", (data) => {
        let target1, target2;

        if (!data.Item) {
            if (data.SourceMemberNumber === Player.MemberNumber) return;
            target1 = getPlayer(data.SourceMemberNumber);
            target2 = getPlayer(data.Character.MemberNumber);
        } else {
            target1 = getPlayer(data.Source);
            target2 = getPlayer(data.Item.Target);
        }
        if (target1 && target2) onCharacterChange(target1, target2);
    });

    ServerSocket.on("ChatRoomSyncItem", (data) => {
        let target;

        if (!data.Item) {
            if (data.SourceMemberNumber === Player.MemberNumber) return;
            target = getPlayer(data.SourceMemberNumber);
        } else {
            if (data.Item.Target !== Player.MemberNumber) return;
            target = getPlayer(data.Source);
        }
        if (target) onPlayerChange(target);
    });

    ServerSocket.on("ChatRoomSyncSingle", (data) => {
        let target;

        if (!data.Item) {
            if (data.SourceMemberNumber === Player.MemberNumber) return;
            target = getPlayer(data.SourceMemberNumber);
        } else {
            if (data.Item.Target !== Player.MemberNumber) return;
            target = getPlayer(data.Source);
        }
        if (target) onPlayerChange(target);
    });

    ServerSocket.on("ChatRoomSyncPose", (data) => {
        const target = getPlayer(data.MemberNumber);
        onCharacterPoseChange(target);
    });

    ServerSocket.on("ChatRoomSyncMemberJoin", (data) => {
        onChatRoomCharacterJoin(
            getPlayer(data.SourceMemberNumber)
        );
    });

    ServerSocket.on("ChatRoomSync", (data) => {
        onChatRoomPlayerJoin(Player);
    });

    ServerSocket.on("ChatRoomSyncMemberLeave", (data) => {
        onChatRoomCharacterLeave(data.SourceMemberNumber);
    });

    ServerSocket.on("ChatRoomMessage", (data) => {
        const target = getPlayer(data.Sender);

        if (
            data.Content === "bccMsg" && 
            data.Sender !== Player.MemberNumber
        ) onChatRoomBCCMessage(target, data.Dictionary);
        if (
            data.Type === "Activity" &&
            data.Sender !== Player.MemberNumber &&
            data.Dictionary[1]?.TargetCharacter === Player.MemberNumber
        ) onChatRoomActivity(target, data?.Dictionary[3]?.ActivityName);        
    });
    
}

function registerHooks() {
    const highPriority = 200;
    const lowPriority = -200;

    BCC.hookFunction("CharacterAppearanceSetItem", highPriority, function (args, next) {
        let C = args[0];
        let Group = args[1];
        let ItemAsset = args[2];
        let NewColor = args[3];
        let DifficultyFactor = args[4];
        let ItemMemberNumber = args[5];
        let Refresh = args[6];

        // Sets the difficulty factor
        if (DifficultyFactor == null) DifficultyFactor = 0;
    
        // Removes the previous if we need to
        const ID = CharacterAppearanceGetCurrentValue(C, Group, "ID");
        var ItemColor;
        if (ID != "None") {
            if (CurrentScreen == "Appearance") {
                ItemColor = CharacterAppearanceGetCurrentValue(C, Group, "Color");
                if ((ItemColor == null || ItemColor == "Default" || ItemColor == "None") && ItemAsset != null && ItemAsset.DefaultColor != null) ItemColor = ItemAsset.DefaultColor;
            }
            C.Appearance.splice(ID, 1);
        } else if (ItemAsset != null) ItemColor = ItemAsset.DefaultColor ? ItemAsset.DefaultColor : ItemAsset.Group.ColorSchema[0];
      
        // Add the new item to the character appearance
        /** @type {null | Item} */
        let NA = null;
        if (ItemAsset != null) {
            NA = {
            Asset: ItemAsset,
            Difficulty: parseInt((ItemAsset.Difficulty == null) ? 0 : bccStorage.autoTighten && ItemMemberNumber === Player.MemberNumber ? 1000 : ItemAsset.Difficulty) + parseInt(DifficultyFactor),
            Color: ((NewColor == null) ? ItemColor : NewColor),
            Property: ItemAsset.CharacterRestricted ? {ItemMemberNumber: ItemMemberNumber == null ? -1 : ItemMemberNumber} : undefined
            };
            ExtendedItemInit(C, NA, false);
            C.Appearance.push(NA);
        }
    
        // Draw the character canvas and calculate the effects on the character
        if (Refresh == null || Refresh) CharacterRefresh(C, false);
        return NA;
    });

    BCC.hookFunction("ChatRoomAllowItem", highPriority, function (args, next) {
        let data = args[0];

        if ((data != null) && (typeof data === "object") && (data.MemberNumber != null) && (typeof data.MemberNumber === "number") && (data.AllowItem != null) && (typeof data.AllowItem === "boolean"))
            if (CurrentCharacter != null && CurrentCharacter.MemberNumber == data.MemberNumber) {
                console.warn(`ChatRoomGetAllowItem mismatch trying to access ${CurrentCharacter.Name} (${CurrentCharacter.MemberNumber})`);
                CurrentCharacter.AllowItem = data.AllowItem || bccStorage.bypassBlocking;
                CharacterSetCurrent(CurrentCharacter);
                if (!data.AllowItem && bccStorage.bypassBlocking) {
                    notify(
                        "You don't have an item permission to interact with this player, but the blocking was ignored",
                        5000
                    );
                }
            }
    });

    BCC.hookFunction("DialogFindPlayer", highPriority, (args, next) => {
        const label = buttonLabels.get(args[0]);
        if (label)
            return label;
        return next(args);
    });
    
    BCC.hookFunction("DrawGetImage", highPriority, (args, next) => {
        const redirect = imageRedirects.get(args[0]);
        if (redirect) {
            args[0] = redirect;
        }
        return next(args);
    });

    BCC.hookFunction("DialogMenuButtonClick", highPriority, (args, next) => {
        // Finds the current icon (Taken from BCX)
        const C = CharacterGetCurrent();
        for (let I = 0; I < getDialogMenuButtons().length; I++) {
            if (MouseIn(1885 - I * 110, 15, 90, 90) && C) {
                const hooks = dialogMenuButtonClickHooks.get(getDialogMenuButtons()[I]);
                if (hooks?.some(hook => hook(C)))
                    return true;
            }
        }
        return next(args);
    });

    
    BCC.hookFunction("DialogMenuButtonBuild", lowPriority, (args, next) => {
        next(args);
        const target = args[0];
        const item = InventoryGet(target, target.FocusGroup.Name);
        if (target.MemberNumber === Player.MemberNumber && target.FocusGroup) {
            if (
                bccStorage.darkMagic.helpless || (bccStorage.abdl.role === "baby" &&
                item?.Asset?.Category?.includes("ABDL")) || (Player.IsRestrained() 
                && playerHasMilkBottle())
            ) {
                const removeIndex = getDialogMenuButtons().indexOf("Remove");
                const struggleIndex = getDialogMenuButtons().indexOf("Struggle");
                const dismountIndex = getDialogMenuButtons().indexOf("Dismount");
                const escapeIndex = getDialogMenuButtons().indexOf("Escape");

                if (removeIndex >= 0) {
                    getDialogMenuButtons()[removeIndex] = "BCC_Remove";
                }
                if (struggleIndex >= 0) {
                    getDialogMenuButtons()[struggleIndex] = "BCC_Struggle";
                }
                if (dismountIndex >= 0) {
                    getDialogMenuButtons()[dismountIndex] = "BCC_Dismount";
                }
                if (escapeIndex >= 0) {
                    getDialogMenuButtons()[escapeIndex] = "BCC_Escape";
                }
            }
        }
    });

    BCC.hookFunction("Player.CanChangeOwnClothes", highPriority, (args, next) => {
        if (bccStorage.asleep || bccStorage.darkMagic.helpless) {
            return false;
        }
        return next(args);
    });

    BCC.hookFunction("Player.IsDeaf", highPriority, (args, next) => {
        if (bccStorage.asleep) return false;
        return next(args);
    });

    BCC.hookFunction("Player.IsBlind", highPriority, (args, next) => {
        if (bccStorage.asleep) return true;
        return next(args);
    });

    BCC.hookFunction("Player.CanWalk", highPriority, (args, next) => {
        if (bccStorage.asleep || bccStorage.darkMagic.helpless) {
            return false;
        }
        return next(args);
    });

    BCC.hookFunction("Player.CanChangeToPose", highPriority, (args, next) => {
        if (bccStorage.asleep || bccStorage.darkMagic.helpless) {
            return false;
        }
        return next(args);
    });

    BCC.hookFunction("ChatRoomCanAttemptStand", highPriority, (args, next) => {
        if (bccStorage.asleep || bccStorage.darkMagic.helpless) {
            return false;
        }
        return next(args);
    });

    BCC.hookFunction("ChatRoomCanAttemptKneel", highPriority, (args, next) => {
        if (bccStorage.asleep || bccStorage.darkMagic.helpless) {
            return false;
        }
        return next(args);
    });

    BCC.hookFunction("ChatRoomFocusCharacter", highPriority, (args, next) => {
        if (bccStorage.asleep) {
            chatSendLocal("You are sleeping!");
            return false;
        }
        return next(args);
    });

    BCC.hookFunction("ServerSend", highPriority, (args, next) => {
        const message = args[0];
        const params = args[1];

        if (message === "ChatRoomChat" && params.Type === "Chat" && params?.Content[0] !== "(") {
            if (bccStorage.asleep) {
                chatSendLocal("You are sleeping!");
                return null; 
            } 
            if (bccStorage.darkMagic.speech === "cat") {
                const pattern = /\b\S+\b/g;
                params.Content = params.Content.replace(pattern, "meow");
                return next(args);
            }
            if (bccStorage.darkMagic.speech === "baby") {
                params.Content = SpeechBabyTalk({
                    Effect: ["RegressedTalk"]
                }, params.Content);
                return next(args);
            }
        } 
        if (message === "ChatRoomCharacterItemUpdate") {
            if (params.Target === Player.MemberNumber) {
                checkSleeping(Player);
                if (bccStorage.skyShield.state) {
                    bccStorage.skyShield.lastAppearance = ServerAppearanceBundle(
                        Player.Appearance
                    );
                }
            } else {
                let curses = bccStorage.curses;
                if (curses[params.Target]) {
                    curses[params.Target] = ServerAppearanceBundle(
                        getPlayer(params.Target).Appearance
                    );
                }
            }
        }
        if (message === "ChatRoomChat" && params.Type === "Activity") {
            let actName = params?.Dictionary[3]?.ActivityName;
            if (actName?.startsWith("BCC_")) {
                let {metadata, substitutions} = ChatRoomMessageRunExtractors(params, Player);
                let actText = ActivityDictionaryText(params.Content);
                actText = CommonStringSubstitute(actText, substitutions ?? []);
                params.Dictionary.push({
                    Tag: "MISSING ACTIVITY DESCRIPTION FOR KEYWORD " + params.Content,
                    Text: actText
                });

                // If action name has a custom action, run it as part of the chain
                var customAction = actData.CustomActionCallbacks.get(actName);
                if (customAction) {
                    customAction(
                        getPlayer(params.Dictionary[1].TargetCharacter), 
                        args, next
                    );
                }
            }
        }
        return next(args);
    });

    BCC.hookFunction("CommandExecute", highPriority*5, (args, next) => {
        const command = args[0].toLowerCase();
        let forbid = false;
        if (bccStorage.darkMagic.helpless) {
            [
                "/bcc free", "/safeword", "/safeworditem",
                "/safewordspecify", "/totalrelease", "/release", 
                "/untie", "/solidity", "/sosbuttons", "/quit",
                "/leave", "/lscg unzonk", "/lscg escape", "/lscg emergency"
            ].forEach(function (cheatCmd) {
                if (command?.startsWith(cheatCmd)) {
                    chatSendCustomAction(`${Player.Name} tried to use cheat command ${cheatCmd}`);
                    forbid = true;
                    return chatSendLocal("You are <!forbidden!> to use cheat commands!");
                }
            });
        }
        if (forbid) return false;

        bccStorage.blockedCommands.forEach(function (forbiddenCmd) {
            if (command?.startsWith(forbiddenCmd)) {
                chatSendCustomAction(`${Player.Name} tried to use forbidden command ${forbiddenCmd}`);
                forbid = true;
                return chatSendLocal("You are <!forbidden!> to use this command!");
            }
        });
        if (forbid) return false;
        return next(args);
    });

    BCC.hookFunction("DrawImageResize", 1, (args, next) => {
        var path = args[0];
        if (!!path && path.indexOf("BCC_") > -1) {
            var activityName = path.substring(path.indexOf("BCC_"));
            activityName = activityName.substring(0, activityName.indexOf(".png"))
            if (actData.CustomImages.has(activityName))
                args[0] = actData.CustomImages.get(activityName);
        }
        return next(args);
    });

    BCC.hookFunction("ActivityCheckPrerequisite", highPriority, (args, next) => {
        var prereqName = args[0];
        if (actData.CustomPrerequisiteFuncs.has(prereqName)) {
            var acting = args[1];
            var acted = args[2];
            var targetGrp = args[3];
            var customPrereqFunc = actData.CustomPrerequisiteFuncs.get(prereqName);
            if (!customPrereqFunc)
                return next(args);
            else {
                try {
                    return customPrereqFunc(acting, acted, targetGrp);
                }
                catch (error) {
                    console.warn(`BCC: Custom Prereq ${prereqName} failed: ${error}`);
                }
            }
        }
        else
            return next(args);
    });

    BCC.hookFunction("ChatRoomMessage", highPriority, (args, next) => {
        const message = args[0];
        if (bccStorage.darkMagic.hallucination) {
            if (message.Sender !== Player.MemberNumber) {
                message.Sender = getRandomPlayer().MemberNumber;
            }
        }
        return next(args);
    });
}

// The code is taken from LSCG
let actData = {
    CustomPrerequisiteFuncs: new Map(),
    CustomIncomingActivityReactions: new Map(),
    CustomActionCallbacks: new Map(),
    CustomPreparseCallbacks: new Map(),
    CustomImages: new Map()
}

function AddCustomPrereq(prereq) {
	if (!actData.CustomPrerequisiteFuncs.get(prereq.Name))
        actData.CustomPrerequisiteFuncs.set(prereq.Name, prereq.Func)
}

function RegisterCustomFuncs(bundle, activity) {
	bundle.CustomPrereqs?.forEach(prereq => {
		if (activity.Prerequisite.indexOf(prereq.Name) == -1)
			activity.Prerequisite.push(prereq.Name);
		AddCustomPrereq(prereq);
	})

	if (!!bundle.CustomReaction && !actData.CustomIncomingActivityReactions.get(activity.Name))
        actData.CustomIncomingActivityReactions.set(activity.Name, bundle.CustomReaction.Func)

	if (!!bundle.CustomImage && !actData.CustomImages.get(activity.Name))
        actData.CustomImages.set(activity.Name, bundle.CustomImage);

	if (!!bundle.CustomAction && !actData.CustomActionCallbacks.get(activity.Name))
        actData.CustomActionCallbacks.set(activity.Name, bundle.CustomAction.Func);

	if (!!bundle.CustomPreparse && !actData.CustomPreparseCallbacks.get(activity.Name))
        actData.CustomPreparseCallbacks.set(activity.Name, bundle.CustomPreparse.Func);
}

function AddTargetToActivity(activity, tgt) {
	tgt.TargetLabel = tgt.TargetLabel ?? activity.Name.substring(4);

	if (tgt.SelfAllowed) {
		if (!activity.TargetSelf)
			activity.TargetSelf = [];
		if (typeof activity.TargetSelf != "boolean" && (activity.TargetSelf).indexOf(tgt.Name) == -1) {
			(activity.TargetSelf).push(tgt.Name);
		}
	}

	if (!tgt.SelfOnly) {
		if (!activity.Target)
			activity.Target = [];

		if (activity.Target.indexOf(tgt.Name) == -1) {
			activity.Target.push(tgt.Name);
		}            
	}

	ActivityDictionary?.push([
		"Label-ChatOther-" + tgt.Name + "-" + activity.Name,
		tgt.TargetLabel
	]);
	ActivityDictionary?.push([
		"ChatOther-" + tgt.Name + "-" + activity.Name,
		tgt.TargetAction
	]);

	if (tgt.SelfAllowed) {
		ActivityDictionary?.push([
			"Label-ChatSelf-" + tgt.Name + "-" + activity.Name,
			tgt.TargetSelfLabel ?? tgt.TargetLabel
		]);
		ActivityDictionary?.push([
			"ChatSelf-" + tgt.Name + "-" + activity.Name,
			tgt.TargetSelfAction ?? tgt.TargetAction
		]);
	}
}

function AddActivity(bundle) {
	if (!bundle.Targets || bundle.Targets.length <= 0)
		return;

	let activity = bundle.Activity;
	activity.Target = activity.Target ?? [];
	activity.Prerequisite = activity.Prerequisite ?? [];
	activity.Name = "BCC_" + activity.Name;

	RegisterCustomFuncs(bundle, bundle.Activity);

	ActivityDictionary?.push([
		"Activity"+activity.Name,
		bundle.Targets[0].TargetLabel ?? activity.Name.substring(4)
	])

	bundle.Targets.forEach(tgt => {
		AddTargetToActivity(activity, tgt);
	});

	ActivityFemale3DCG.push(activity);
	ActivityFemale3DCGOrdering.push(activity.Name);
}

function registerActivities() {
    actData.CustomPrerequisiteFuncs.set("hasBCC", function (target1, target2, group) {
        return (
            target2.MemberNumber === Player.MemberNumber ? true : 
            bccCharactersIds.includes(target2.MemberNumber)
        );
    });
    actData.CustomPrerequisiteFuncs.set("hasWand", function (target1, target2, group) {
        return playerHasWand();
    });
    actData.CustomPrerequisiteFuncs.set("isNotAsleep", function (target1, target2, group) {
        return !target2.OnlineSharedSettings.BCC.asleep;
    });
    actData.CustomPrerequisiteFuncs.set("isNotLicking", function (target1, target2, group) {
        return !target2.OnlineSharedSettings.BCC.darkMagic.licking;
    });
    actData.CustomPrerequisiteFuncs.set("allowSleeping", function (target1, target2, group) {
        return target2.OnlineSharedSettings.BCC.sleepingEnabled;
    });
    actData.CustomPrerequisiteFuncs.set("hasSkyShield", function (target1, target2, group) {
        return target2.OnlineSharedSettings.BCC.skyShield.state;
    });
    actData.CustomPrerequisiteFuncs.set("isNotHelpless", function (target1, target2, group) {
        return !target2.OnlineSharedSettings.BCC.darkMagic.helpless;
    });
    actData.CustomPrerequisiteFuncs.set("speechIsNotChanged", function (target1, target2, group) {
        return !target2.OnlineSharedSettings.BCC.darkMagic.speech;
    });
    actData.CustomPrerequisiteFuncs.set("isNotHallucination", function (target1, target2, group) {
        return !target2.OnlineSharedSettings.BCC.darkMagic.hallucination;
    });


    AddActivity({
        Activity: {
            Name: "forceLickLegs",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: [
                "UseArms", "hasBCC", "hasWand",
                "isNotAsleep", "isNotLicking"
            ]
        },
        Targets: [
            {   
                TargetLabel: "Force lick your legs",
                Name: "ItemArms",
                SelfAllowed: false,
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter"
            }
        ],
        CustomImage: `${staticPath}/images/lick-legs.png`
    });
    AddActivity({
        Activity: {
            Name: "putToSleep",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: [
                "UseArms", "hasBCC", "hasWand",
                "isNotAsleep", "allowSleeping",
                "isNotLicking"
            ]
        },
        Targets: [
            {   
                TargetLabel: "Put to sleep",
                Name: "ItemArms",
                SelfAllowed: false,
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter"
            }
        ],
        CustomImage: `${staticPath}/images/put-to-sleep.png`
    });
    AddActivity({
        Activity: {
            Name: "removeEnchantments",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: ["UseArms", "hasBCC", "hasWand"]
        },
        Targets: [
            {   
                Name: "ItemArms",
                SelfAllowed: true,
                TargetLabel: "Remove enchantments",
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter",
                TargetSelfAction: "SourceCharacter uses dark magic on yourself"
            }
        ],
        CustomAction: {
            Func: (target, args, next) => {
                if (target.MemberNumber !== Player.MemberNumber) return;
                setTimeout(() => {
                    playerRemoveEnchantments();
                    chatSendCustomAction(`All negative effects were removed from ${Player.Name}`);
                }, 500);
            }
        },
        CustomImage: `${staticPath}/images/rm-enchantments.png`
    });
    AddActivity({
        Activity: {
            Name: "destroySkyShield",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: ["UseArms", "hasBCC", "hasWand", "hasSkyShield"]
        },
        Targets: [
            {   
                TargetLabel: "Destroy the sky shield",
                Name: "ItemArms",
                SelfAllowed: false,
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter"
            }
        ],
        CustomImage: `${staticPath}/images/destroy-sky-shield.png`
    });
    AddActivity({
        Activity: {
            Name: "makeHelpless",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: ["useArms", "hasBCC", "hasWand", "isNotHelpless"]
        },
        Targets: [
            {   
                TargetLabel: "Make helpless",
                Name: "ItemArms",
                SelfAllowed: false,
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter"
            }
        ],
        CustomImage: `${staticPath}/images/make-helpless.png`
    });
    AddActivity({
        Activity: {
            Name: "makeHallucination",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: ["useArms", "hasBCC", "hasWand", "isNotHallucination"]
        },
        Targets: [
            {   
                TargetLabel: "Make hallucination",
                Name: "ItemArms",
                SelfAllowed: false,
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter"
            }
        ],
        CustomImage: `${staticPath}/images/make-hallucination.png`
    });
    AddActivity({
        Activity: {
            Name: "makeCatSpeech",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: ["useArms", "hasBCC", "hasWand", "speechIsNotChanged"]
        },
        Targets: [
            {   
                TargetLabel: "Make cat speech",
                Name: "ItemArms",
                SelfAllowed: false,
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter"
            }
        ],
        CustomImage: `${staticPath}/images/make-cat-speech.png`
    });
    AddActivity({
        Activity: {
            Name: "makeBabySpeech",
            MaxProgress: 70,
            MaxProgressSelf: 70,
            Prerequisite: ["useArms", "hasBCC", "hasWand", "speechIsNotChanged"]
        },
        Targets: [
            {   
                TargetLabel: "Make baby speech",
                Name: "ItemArms",
                SelfAllowed: false,
                TargetAction: "SourceCharacter uses dark magic on TargetCharacter"
            }
        ],
        CustomImage: `${staticPath}/images/make-baby-speech.png`
    });
}

function filterAppearance(appearance1, appearance2, filter) {
    if (filter === 0) {
        // body + items
        appearance1 = appearance1.filter((item) => {
            return bodyGroupNames.includes(item.Group) || item.Group.startsWith("Item");
        });

        // clothes
        appearance2 = appearance2.filter((item) => {
            return !bodyGroupNames.includes(item.Group) && !item.Group.startsWith("Item");
        });
    } else if (filter === 1) {
        let usedItemsGroups = [];
        const padlocks = [
            "OwnerPadlock", "OwnerTimerPadlock", "LoversPadlock",
            "LoversTimerPadlock", "PasswordPadlock", "TimerPasswordPadlock"
        ];

        // body or body + some items if safe mode
        appearance1 = appearance1.filter((item) => {
            if (
                bccStorage.safeMode &&
                padlocks.includes(item.Property?.LockedBy)
            ) {
                usedItemsGroups.push(item.Group);
                return true;
            } else return bodyGroupNames.includes(item.Group);
        });

        // clothes + items or clothes + some items if safe mode
        appearance2 = appearance2.filter((item) => {
            if (bccStorage.safeMode) {
                return !usedItemsGroups.includes(item.Group) && !bodyGroupNames.includes(item.Group)
            } else return !bodyGroupNames.includes(item.Group);
        });
    }
    return appearance1.concat(appearance2);
} 

function chatSendLocal(message, align = "center", bgColor = purpleColor) {
    let style;
    if (align === "center") {
        style = `font-size: 2vw; margin-top: 4px; margin-bottom: 4px; background: ${bgColor}; color: white; padding: 8px; text-align: center;`;
    } else if (align === "left") {
        style = `font-size: 2vw; margin-top: 4px; margin-bottom: 4px; background: ${bgColor}; color: white;`;
    }

    message = message
        .replaceAll("<!", `<span style='color: ${pinkColor};'>`).replaceAll("!>", "</span>")
        .replaceAll("<%", `<span style='background: ${blueColor}; padding-left: 2px; padding-right: 2px;'>`).replaceAll("%>", "</span>");

    const msgElement = document.createElement("div");
    msgElement.innerHTML = message;
    msgElement.style = style;
    document.querySelector("#TextAreaChatLog").appendChild(msgElement);
    ElementScrollToEnd("TextAreaChatLog");
}

function chatSendCustomAction(message) {
    const regexp1 = new RegExp("(.*?) tried to change (.*?) restraints, but all the changes were reset");
    const regexp2 = new RegExp("(.*?) tried to touch (.*?), but the (.*?) sky shield prevented her");

    if (
        regexp1.test(message) || regexp2.test(message) 
    ) {
        if (Date.now() - lastCustomAction < 500 ) return;
        lastCustomAction = Date.now();
    }

    ServerSend("ChatRoomChat", {
        Content: "BCC_ACTION_MESSAGE",
        Type: "Action",
        Dictionary: [
            {
                Tag: "MISSING PLAYER DIALOG: BCC_ACTION_MESSAGE",
                Text: message 
            }
        ],
    });
}

function chatSend(message) {
    ServerSend("ChatRoomChat", {
        Content: message,
        Type: "Chat"
    });
}

function chatSendBCCMessage(msg, _data = undefined, targetNumber = undefined) {
    let data = {
        Content: "bccMsg",
        Dictionary: {
            msg: msg,
        },
        Type: "Hidden"
    }
    if (_data) data.Dictionary.data = _data;
    if (targetNumber) data.Target = targetNumber;
    ServerSend("ChatRoomChat", data);
}

function notify(message, duration = 3000) {
    ServerBeep = {
        Timer: CommonTime() + duration,
        Message: message,
    };
}

function addCurse(target) {
    let curses = bccStorage.curses;
    curses[target.MemberNumber] = ServerAppearanceBundle(target.Appearance);
}

function removeCurse(target) {
    let curses = bccStorage.curses;
    delete curses[target.MemberNumber];
}

function addForceKneel(target) {
    bccStorage.forceKneel.push(target.MemberNumber);
    if (!target.ActivePose.includes("Kneel")) {
        ServerSend("ChatRoomCharacterUpdate", {
            ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
            ActivePose: [...target.ActivePose, "Kneel"],
            Appearance: ServerAppearanceBundle(
                target.Appearance
            )
        });
    }
}

function removeForceKneel(target) {
    bccStorage.forceKneel.splice(
        bccStorage.forceKneel.indexOf(target.MemberNumber), 1
    );
    let activePose = target.ActivePose;
    activePose.splice(
        activePose.indexOf("Kneel"), 1
    );
    ServerSend("ChatRoomCharacterUpdate", {
        ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
        ActivePose: activePose,
        Appearance: ServerAppearanceBundle(
            target.Appearance
        )
    });
}

function addBCCMember(target) {
    chatSendLocal(`<!${target.Name}!> has BCC`);
    chatSendBCCMessage("Hello", undefined, target.MemberNumber);
    bccCharactersIds.push(target.MemberNumber)
}

function removeBCCMember(targetNumber) {
    bccCharactersIds.splice(
        bccCharactersIds.indexOf(
            targetNumber
        ), 1
    );
}

function clearBCCMembers() {
    bccCharactersIds = [];
}

function getRandomBCCBtnId() {
    return `bccBtn-${Date.now()}`;
}

function getRandomPlayer() {
    return ChatRoomCharacter[Math.floor(Math.random() * ChatRoomCharacter.length)];
}

function getDialogMenuButtons() {
	return DialogMenuButton;
}

function registerButton(name, label, icon, fn) {
	imageRedirects.set(`Icons/${name}.png`, icon);
	buttonLabels.set(name, label);

	let hooks = dialogMenuButtonClickHooks.get(name);
	if (!hooks) {
		hooks = [];
		dialogMenuButtonClickHooks.set(name, hooks);
	}
	if (!hooks.includes(fn)) {
		hooks.push(fn);
	}
}

function playerSleep() {
    if (Player.CanKneel()) {
        CharacterSetActivePose(Player, "Kneel", true);
    }
    bccStorage.asleep = true;
    CharacterSetFacialExpression(Player, "Eyes", "Closed");
    CharacterSetFacialExpression(Player, "Emoticon", "Sleep");
    ChatRoomCharacterUpdate(Player);
    
    const a = setInterval(function () {
        if (!bccStorage.asleep) {
            CharacterSetFacialExpression(Player, "Eyes", null);
            CharacterSetFacialExpression(Player, "Emoticon", null);
            ChatRoomCharacterUpdate(Player);
            return clearInterval(a);
        }
        CharacterSetFacialExpression(Player, "Eyes", "Closed");
        CharacterSetFacialExpression(Player, "Emoticon", "Sleep");
        ChatRoomCharacterUpdate(Player);
    }, 5000);

    chatSendCustomAction(`${Player.Name} fell asleep, only hot kiss or hard spanking can wake her up`);
    chatSendLocal("You fell into a deep sleep...");
}

function playerAwake() {
    bccStorage.asleep = false;
    CharacterSetFacialExpression(Player, "Eyes", null);
    CharacterSetFacialExpression(Player, "Emoticon", null);
    ChatRoomCharacterUpdate(Player);
    chatSendLocal("You're awake");
}

class ChatCommands {
    constructor() {
        this.free = {
            name: "free",
            description: "Free yourself or target",
            args: "<![target]!>",
            danger: 0,
            action: (text) => {
                let target = Player;

                if (text) {
                    target = getPlayer(text);
                    if (!target) return chatSendLocal(`Example: /bcc ${this.free.name} ${this.free.args}`);
                    chatSendLocal(`You have successfully used magic and freed <!${target.Name}!>!`);
                } else {
                    chatSendLocal(`You have successfully used magic and freed <!yourself!>!`);
                }
                CharacterRelease(target);
                ChatRoomCharacterUpdate(target);
            }
        }
        this.nick = {
            name: "nick",
            description: "Change nickname",
            args: "<![nickname]!>",
            danger: 0,
            action: (text) => {
                let nickname = text;

                if (!nickname) return chatSendLocal(`Example: /bcc ${this.nick.name} ${this.nick.args}`);
                nickname = nickname[0].toUpperCase() + nickname.slice(1);
                Player.Nickname = nickname;
                ServerSend("AccountUpdate", {Nickname: nickname});
                chatSendLocal("Your nickname was successfully changed!");
            }
        }
        this.clone = {
            name: "clone",
            description: "Copy someone's appearance",
            args: "<![target]!>",
            danger: 0,
            action: (text) => {
                const target = getPlayer(text);
                const nickname = target.Nickname ? target.Nickname : target.Name;

                if (!target) return chatSendLocal(`Example: /bcc ${this.clone.name} ${this.clone.args}`);
                Player.Nickname = nickname;
                Player.LabelColor = target.LabelColor;
                ServerSend("AccountUpdate", {Nickname: nickname, LabelColor: target.LabelColor});  
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: Player.OnlineID,
                    ActivePose: target.ActivePose, 
                    Appearance: ServerAppearanceBundle(target.Appearance)
                });
                chatSendLocal(`You have successfully cloned <!${target.Name}!>!`);
            }
        }
        this.import = {
            name: "import",
            description: "import target1's clothing/restraints on target2",
            args: "<![target1]!>, <![target2]!>, <![filter]!>",
            danger: 1,
            action: (text) => {
                const args = getArgs(text);
                const target1 = getPlayer(args[0]);
                const target2 = getPlayer(args[1]);
                const filter = parseInt(args[2]);

                if (!target1 || !target2 || ![0, 1].includes(filter)) {
                    return chatSendLocal(`Example: /bcc ${this.import.name} ${this.import.args}`);
                }
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: (target2.ID === 0) ? target2.OnlineID : target2.AccountName.replace("Online-", ""),
                    ActivePose: [], 
                    Appearance: filterAppearance(
                        ServerAppearanceBundle(target2.Appearance),
                        ServerAppearanceBundle(target1.Appearance),
                        filter
                    )
                });
                chatSendLocal(`You have successfully imported <!${filter === 0 ? "clothing" : "restraints"}!> from <!${target1.Name}!> to <!${target2.Name}!>!`);
            }
        }
        this.bcximport = {
            name: "bcximport",
            description: "import clothing/restraints on target using bcx code from clipboard",
            args: "<![target]!>, <![filter]!>",
            danger: 1,
            action: async (text) => {
                const args = getArgs(text)
                const target = getPlayer(args[0]);
                const filter = parseInt(args[1]);

                if (![0, 1].includes(filter) || (!target)) {
                    return chatSendLocal(`Example: /bcc ${this.bcximport.name} ${this.bcximport.args}`);
                }
                const appearance = filterAppearance(
                    ServerAppearanceBundle(target.Appearance),
                    JSON.parse(
                        LZString.decompressFromBase64(await navigator.clipboard.readText())
                    ), filter
                )
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
                    ActivePose: [], 
                    Appearance: appearance
                }); 
                chatSendLocal(`You have successfully imported <!${filter === 0 ? "clothing" : "restraints"}!> on <!${target.Name}!>!`);
            }
        }
        this.importall = {
            name: "importall",
            description: "import target's clothing/restraints on everyone in the room",
            args: "<![target]!>, <![filter]!>",
            danger: 2,
            action: (text) => {
                const args = getArgs(text);
                const target1 = getPlayer(args[0]);
                const filter = parseInt(args[1]);

                if (!target1 || ![0, 1].includes(filter)) {
                    return chatSendLocal(`Example: /bcc ${this.importall.name} ${this.importall.args}`);
                }
                ChatRoomCharacter.forEach((target2) => {
                    ServerSend("ChatRoomCharacterUpdate", {
                        ID: (target2.ID === 0) ? target2.OnlineID : target2.AccountName.replace("Online-", ""),
                        ActivePose: [], 
                        Appearance: filterAppearance(
                            ServerAppearanceBundle(target2.Appearance),
                            ServerAppearanceBundle(target1.Appearance),
                            filter
                        )
                    });
                });
                chatSendLocal(`You have successfully imported <!${filter === 0 ? "clothing" : "restraints"}!> from <!${target1.Name}!> to <!everyone in the room!>!`);
            }
        }
        this.bcximportall = {
            name: "bcximportall",
            description: "import clothing/restraints on everyone in the room using bcx code from clipboard",
            args: "<![filter]!>",
            danger: 2,
            action: async (text) => {
                const filter = parseInt(text);

                if (![0, 1].includes(filter)) {
                    return chatSendLocal(`Example: /bcc ${this.bcximportall.name} ${this.bcximportall.args}`);
                }
                const bcxCode = await navigator.clipboard.readText();
                ChatRoomCharacter.forEach((target) => {
                    const appearance = filterAppearance(
                        ServerAppearanceBundle(target.Appearance),
                        JSON.parse(
                            LZString.decompressFromBase64(bcxCode)
                        ), filter
                    );
                    ServerSend("ChatRoomCharacterUpdate", {
                        ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
                        ActivePose: [], 
                        Appearance: appearance
                    });
                });
                chatSendLocal(`You have successfully imported <!${filter === 0 ? "clothing" : "restraints"}!> on <!everyone in the room!>!`);
            }
        }
        this.skyshield = {
            name: "skyshield",
            description: "Turns on/off the sky shield",
            args: "",
            danger: 0,
            action: (text) => {
                const skyShieldState = bccStorage.skyShield.state;

                if (skyShieldState) {
                    turnOffSkyShield();
                } else {
                    turnOnSkyShield();
                }
                chatSendLocal(`You have successfully <!${skyShieldState ? "deactivated" : "activated"}!> sky shield!`);
            }
        }
        this.curse = {
            name: "curse",
            description: "Curse target",
            args: "<![target]!>",
            danger: 1,
            action: (text) => {
                const target = getPlayer(text);
                const curses = bccStorage.curses;

                if (!target) return chatSendLocal(`Example: /bcc ${this.curse.name} ${this.curse.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.curse.name} ${this.curse.args}`);
                if (
                    bccCharactersIds.includes(target.MemberNumber) &&
                    target.OnlineSharedSettings?.BCC?.skyShield?.state
                ) {
                    return chatSendLocal(`${target.Name}'s sky shield is activated now!`);
                }

                if (!curses[target.MemberNumber]) {
                    addCurse(target);
                    chatSendCustomAction(`${Player.Name} cursed ${target.Name}`);
                    chatSendLocal(`You have successfully cursed <!${target.Name}!>!`);              
                } else {
                    removeCurse(target);
                    chatSendCustomAction(`${Player.Name} removed curse from ${target.Name}`);
                    chatSendLocal(`You have successfully removed curse from <!${target.Name}!>!`);              
                }
            }
        }
        this.steal = {
            name: "steal",
            description: "Steal item from target's hand",
            args: "<![target]!>",
            danger: 0,
            action: (text) => {
                const target = getPlayer(text);
                if (!target) return chatSendLocal(`Example: /bcc ${this.steal.name} ${this.steal.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.steal.name} ${this.steal.args}`);

                const item = InventoryGet(target, "ItemHandheld");
                if (!item) return chatSendLocal(`Example: /bcc ${this.steal.name} ${this.steal.args}`);

                let itemName;
                const filter = (el) => el.Group !== "ItemHandheld";
                if (item.Craft) itemName = item.Craft.Name;
                else itemName = item.Asset.Description;
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
                    ActivePose: target.ActivePose, 
                    Appearance: ServerAppearanceBundle(
                        target.Appearance
                    ).filter(filter)
                });

                let playerAppearance = ServerAppearanceBundle(
                    Player.Appearance
                ).filter(filter);
                playerAppearance.push({
                    Group: "ItemHandheld",
                    Name: item.Asset.Name,
                    Color: item.Color,
                    Property: item.Property ? item.Property : {},
                    Craft: item.Craft,
                    Difficulty: item.Difficulty
                });
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: (Player.ID === 0) ? Player.OnlineID : Player.AccountName.replace("Online-", ""),
                    ActivePose: Player.ActivePose, 
                    Appearance: playerAppearance
                });
                chatSendCustomAction(`${Player.Name} stole ${itemName} from ${target.Name}`);
                chatSendLocal(`You have successfully stole <!${itemName}!> from <!${target.Name}!>!`);
            }
        }
        this.arrest = {
            name: "arrest",
            description: "Arrest target",
            args: "<![target]!>",
            danger: 1,
            action: (text) => {
                const target = getPlayer(text);

                if (!target) return chatSendLocal(`Example: /bcc ${this.arrest.name} ${this.arrest.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.arrest.name} ${this.arrest.args}`);

                const prisonerAppearance = filterAppearance(
                    ServerAppearanceBundle(target.Appearance),
                    assets.prisoner, 1
                );
                const copAppearance = filterAppearance(
                    ServerAppearanceBundle(Player.Appearance),
                    assets.cop, 1
                );
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
                    ActivePose: [], 
                    Appearance: prisonerAppearance
                });
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: (Player.ID === 0) ? Player.OnlineID : Player.AccountName.replace("Online-", ""),
                    ActivePose: [], 
                    Appearance: copAppearance
                });
                chatSendCustomAction(`${Player.Name} puts ${target.Name} in shackles and locks her in a cage`);
                setTimeout(() => chatSendCustomAction(`${Player.Name} arrested ${target.Name}`), 1000);    
                chatSendLocal(`You have successfully arrested <!${target.Name}!>!`);
            }
        }
        this.permbonus = {
            name: "permbonus",
            description: "Get permanent +5 boost for each skill",
            args: "",
            danger: 0,
            action: (text) => {  
                let skills = Player.Skill;
                bccStorage.permBonus = !bccStorage.permBonus;

                if (bccStorage.permBonus) {
                    skills.forEach((skill) => {
                        skill.ModifierLevel = 5;
                        skill.ModifierTimeout = Date.now() + 3600000;
                    });
                    const id = setInterval(() => {
                        if (!bccStorage.permBonus) {
                            return clearInterval(id);
                        }
                        let skills = Player.Skill;
                        skills.forEach((skill) => {
                            skill.ModifierLevel = 5;
                            skill.ModifierTimeout = Date.now() + 3600000;
                        });
                        ServerSend("AccountUpdate", {
                            Skill: skills
                        });
                    }, 100000);
                } else {
                    skills.forEach((skill) => {
                        delete skill.ModifierLevel;
                        delete skill.ModifierTimeout;
                    });
                }
                ServerSend("AccountUpdate", {
                    Skill: skills
                });
                chatSendLocal(`You have successfully <!${bccStorage.permBonus ? "activated" : "deactivated"}!> permanent bonus!`);
            }
        }
        this.tighten = {
            name: "tighten",
            description: "Tighten target's restraints",
            args: "<![target]!>",
            danger: 1,
            action: (text) => {
                const target = getPlayer(text);
                if (!target) return chatSendLocal(`Example: /bcc ${this.tighten.name} ${this.tighten.args}`);

                let appearance = ServerAppearanceBundle(
                    target.Appearance
                );
                appearance.map(function (item) {
                    if (item.Group.startsWith("Item")) {
                        item.Difficulty = 1000;
                    }
                });
                ServerSend("ChatRoomCharacterUpdate", {
                    ID: (target.ID === 0) ? target.OnlineID : target.AccountName.replace("Online-", ""),
                    ActivePose: target.ActivePose, 
                    Appearance: appearance
                });
                chatSendLocal(`You have successfully tightened <!${target.Name}'s!> restraints!`)
            }
        }
        this.autotighten = {
            name: "autotighten",
            description: "Automatically tighten restraints when you use them on someone",
            args: "",
            danger: 1,
            action: (text) => {
                bccStorage.autoTighten = !bccStorage.autoTighten;
                chatSendLocal(`You have successfully <!${bccStorage.autoTighten ? "activated" : "deactivated"}!> auto tighten!`)
            }
        }
        this.kneel = {
            name: "kneel",
            description: "Force target to kneel",
            args: "<![target]!>",
            danger: 1,
            action: (text) => {
                const target = getPlayer(text);
                if (!target) return chatSendLocal(`Example: /bcc ${this.kneel.name} ${this.kneel.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.kneel.name} ${this.kneel.args}`);

                const forceKneel = bccStorage.forceKneel;
                if (forceKneel.includes(target.MemberNumber)) {
                    removeForceKneel(target);
                    chatSendLocal(`You have successfully allowed <!${target.Name}!> to stand up!`)
                } else {
                    addForceKneel(target);
                    chatSendLocal(`You have successfully forced <!${target.Name}!> to kneel!`);
                }
            }
        }
        this.say = {
            name: "say",
            description: "Send a message on behalf of another user <%Target's BCC required%> <%Not blacklist required%>",
            args: "<![target]!>, <![text]!>",
            danger: 1,
            action: (text) => {
                const args = getArgs(text);
                const target = getPlayer(args[0]);
                const message = text.split(",").slice(1).join(",").trim();

                if (!target) return chatSendLocal(`Example: /bcc ${this.say.name} ${this.say.args}`);
                if (!message) return chatSendLocal(`Example: /bcc ${this.say.name} ${this.say.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.say.name} ${this.say.args}`);
                if (!bccCharactersIds.includes(target.MemberNumber)) {
                    return chatSendLocal(`<!${target.Name}!> doesn't have BCC!`);
                }
                if (target.BlackList.includes(Player.MemberNumber)) {
                    return chatSendLocal(`You are in <!${target.Name}'s!> blacklist!`);
                }
                chatSendBCCMessage("Say", {
                    message: message
                }, target.MemberNumber);
                chatSendLocal(`The message was successfully sent on behalf of <!${target.Name}!>!`);
            }
        }
        this.hsay = {
            name: "hsay",
            description: "Send a hidden message on behalf of another user, who will not see this message <%Targets's BCC required%> <%Not blacklist required%>",
            args: "<![target]!>, <![text]!>",
            danger: 1,
            action: (text) => {
                const args = getArgs(text);
                const target = getPlayer(args[0]);
                const message = text.split(",").slice(1).join(",").trim();

                if (!target) return chatSendLocal(`Example: /bcc ${this.hsay.name} ${this.hsay.args}`);
                if (!message) return chatSendLocal(`Example: /bcc ${this.hsay.name} ${this.hsay.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.hsay.name} ${this.hsay.args}`);
                if (!bccCharactersIds.includes(target.MemberNumber)) {
                    return chatSendLocal(`<!${target.Name}!> doesn't have BCC!`);
                }
                if (target.BlackList.includes(Player.MemberNumber)) {
                    return chatSendLocal(`You are in <!${target.Name}'s!> blacklist!`);
                }
                chatSendBCCMessage("Hidden say", {
                    message: message
                }, target.MemberNumber);
                chatSendLocal(`The hidden message was successfully sent on behalf of <!${target.Name}!>!`);
            }
        }
        this.safemode = {
            name: "safemode",
            description: "Turn on/off safe mode (Safe mode prevents the loss of items with mistress, lover and password locks when BCC is changing appearance)",
            args: "",
            danger: 0,
            action: (text) => {
                let safeMode = bccStorage.safeMode;
                bccStorage.safeMode = !safeMode;
                chatSendLocal(`You have successfully <!${safeMode ? "deactivated" : "activated"}!> safe mode!`);
            }
        }
        this.bypassblocking = {
            name: "bypassblocking",
            description: "Turn on/off bypass blocking (Client side! Allows you to interact with players, even if you don't have item permission to do this. It is well used to perform sexual actions on anyone who blocks it)",
            args: "",
            danger: 1,
            action: (text) => {
                let bypassBlocking = bccStorage.bypassBlocking;
                bccStorage.bypassBlocking = !bypassBlocking;
                chatSendLocal(`You have successfully <!${bypassBlocking ? "deactivated" : "activated"}!> bypass blocking!`);
            }
        }
        this.baby = {
            name: "baby",
            description: "Send request to become baby <%Target's BCC required%>",
            args: "<![target]!>",
            danger: 0,
            action: (text) => {
                const args = getArgs(text);
                const target = getPlayer(args[0]);

                if (!target) return chatSendLocal(`Example: /bcc ${this.baby.name} ${this.baby.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.baby.name} ${this.baby.args}`);
                if (!bccCharactersIds.includes(target.MemberNumber)) {
                    return chatSendLocal(`<!${target.Name}!> doesn't have BCC!`);
                }
                if (target.OnlineSharedSettings.BCC.abdl.role === "baby") {
                    return chatSendLocal(`<!${target.Name}!> already has mommy!`);
                }
                if (bccStorage.abdl.role === "baby") {
                    return chatSendLocal(`You already have mommy!`);
                }
                if (target.OnlineSharedSettings.BCC.abdl.requests) {
                    if (target.OnlineSharedSettings.BCC.abdl.requests.includes(Player.MemberNumber)) {
                        return chatSendLocal(`You already sent baby request to <!${target.Name}!>!`);
                    }
                }
                chatSendBCCMessage("Baby request", undefined, target.MemberNumber);
                chatSendLocal(`You have successfully sent the baby request to <!${target.Name}!>!`);
            }
        }
        this.babycontrol = {
            name: "babycontrol",
            description: "Open baby control menu <%Target's BCC required%>",
            args: "<![target]!>",
            danger: 0,
            action: (text) => {
                const args = getArgs(text);
                const target = getPlayer(args[0]);

                if (!target) return chatSendLocal(`Example: /bcc ${this.babycontrol.name} ${this.babycontrol.args}`);
                if (target.MemberNumber === Player.MemberNumber) return chatSendLocal(`Example: /bcc ${this.babycontrol.name} ${this.babycontrol.args}`);
                if (!bccCharactersIds.includes(target.MemberNumber)) {
                    return chatSendLocal(`<!${target.Name}!> doesn't have BCC!`);
                }
                if (target.OnlineSharedSettings.BCC.abdl.role !== "baby") {
                    return chatSendLocal(`<!${target.Name}!> is not baby!`);
                }
                if (target.OnlineSharedSettings.BCC.abdl.mommy?.id !== Player.MemberNumber) {
                    return chatSendLocal(`<!${target.Name}!> is not your baby!`);
                }
                const btnId = getRandomBCCBtnId();
                chatSendLocal(`Baby control of <!${target.Name}!><br><br><button id="${btnId}" style="${btnStyle}" onclick="window.bccBtnCallbacks.releaseBaby(${target.MemberNumber}, '${btnId}')">Release baby</button>`);      
            }
        }
        this.sleeping = {
            name: "sleeping",
            description: "Turn on/off sleeping system (When this option is activated, you can be put to sleep using a bottle of milk or dark magic)",
            args: "",
            danger: 0,
            action: (text) => {
                if (bccStorage.abdl.role === "baby") {
                    return chatSendLocal("Babies are <!not allowed!> to change this option!");
                }
                bccStorage.sleepingEnabled = !bccStorage.sleepingEnabled;
                chatSendLocal(`You have successfully <!${bccStorage.sleepingEnabled ? "activated" : "deactivated"}!> sleeping system!`);
            }
        }
        this.profile = {
            name: "profile",
            description: "View the target's BCC profile <%Target's BCC required%>",
            args: "<![target]!>",
            danger: 0,
            action: (text) => {
                let target = getPlayer(text);
                let bccData;
                if (target) {
                    if (!bccCharactersIds.includes(target.MemberNumber)) {
                        return chatSendLocal(`<!${target.Name}!> doesn't have BCC!`);
                    }
                    if (!target.OnlineSharedSettings.BCC) {
                        return chatSendLocal(`<!${target.Name}!> doesn't have BCC data!`);
                    }
                    bccData = target.OnlineSharedSettings.BCC;
                } else {
                    target = Player;
                    bccData = bccStorage;
                }
                
                const greenField = (text) => `<span style='text-align: center; padding: 1px 5px;border-radius: 5px; color: lime; background: #8843c5;'>${text}</span>`;
                const redField = (text) => `<span style='text-align: center; padding: 1px 5px; border-radius: 5px; color: red; background: #8843c5;'>${text}</span>`;
                const commonField = (text) => `<span style='text-align: center; padding: 1px 5px; border-radius: 5px; background: #8843c5;'>${text}</span>`;
                const bccDataBox = (text) => `<div style='padding: 3px;'>★ ${text}</div>`;
                const bccDataBoxItem = (text) => `<div style='padding: 2px 2px 2px 12px;'>${text}</div>`;

                let curses = Object.keys(bccData.curses).map((id) => {
                    const curseTarget = getPlayer(id);
                    if (curseTarget) return `${curseTarget.Name} (${id})`;
                    else return `${id}`;
                }).join(", ");

                let blockedCommands = bccData.blockedCommands.join(", ");
                let message =`<p style='text-align: center;'>❣ ${target.Name} (${target.MemberNumber}) ❣</p>`
                message += bccDataBox(`BCC version: ${commonField(bccData.version)}`);
                message += bccDataBox("Sky shield:");
                message += bccDataBoxItem(`State: ${bccData.skyShield.state ? greenField("on") : redField("off")}`);
                message += bccDataBoxItem(`Health: <!${bccData.skyShield.health}!>`);
                message += bccDataBox(`Curses: ${commonField(curses)}`);
                message += bccDataBox(`Forbidden commands: ${commonField(blockedCommands)}`);
                message += bccDataBox(`Sleeping system: ${bccData.sleepingEnabled ? greenField("on") : redField("off")}`);
                message += bccDataBox(`Permanent bonus: ${bccData.permBonus ? greenField("on") : redField("off")}`);
                message += bccDataBox(`Auto tighten: ${bccData.autoTighten ? greenField("on") : redField("off")}`);
                message += bccDataBox(`Bypass blocking: ${bccData.bypassBlocking ? greenField("on") : redField("off")}`);

                if (bccData.abdl.role) {
                    if (bccData.abdl.role === "mommy") {
                        let babies = bccData.abdl.babies.map((baby) => {
                            return `${baby.name} (${baby.id})`;
                        }).join(", ");
                        message += bccDataBox("ABDL:");
                        message += bccDataBoxItem(`Role: ${commonField("mommy")}`);
                        message += bccDataBoxItem(`Babies: ${commonField(babies)}`);
                    } else {
                        message += bccDataBox("ABDL:");
                        message += bccDataBoxItem(`Role: ${commonField("baby")}`);
                        message += bccDataBoxItem(`Mommy: ${commonField(`${bccData.abdl.mommy.name} (${bccData.abdl.mommy.id})`)}`);
                    }
                }
                chatSendLocal(message, "left");
            }
        }
        this.commands = {
            name: "commands",
            description: "Allow or forbid certain commands to a target <%Target's BCC required%> <%Not blacklist required%>",
            args: "<![target]!>, <!allow!>/<!forbid!>, <![commands]!>, ",
            danger: 1,
            action: (text) => {
                const args = getArgs(text);
                const target = getPlayer(args[0]);
                const action = args[1]
                const commands = args.slice(2);

                if (
                    !target || !["allow", "forbid"].includes(action) ||
                    (commands[0] === "" && commands.length === 1) || 
                    target.MemberNumber === Player.MemberNumber
                ) {
                    return chatSendLocal(
                        `Example: /bcc ${this.commands.name} ${this.commands.args}`
                    );
                }
                if (!bccCharactersIds.includes(target.MemberNumber)) {
                    return chatSendLocal(`<!${target.Name}!> doesn't have BCC!`);
                }
                if (target.BlackList.includes(Player.MemberNumber)) {
                    return chatSendLocal(`You are in <!${target.Name}'s!> blacklist!`);
                }
                let actionStr;
                if (action === "allow") {
                    actionStr = "allowed";
                    chatSendBCCMessage("Allow commands", {
                        commands: commands
                    }, target.MemberNumber);
                } else {
                    actionStr = "forbade";
                    chatSendBCCMessage("Forbid commands", {
                        commands: commands
                    }, target.MemberNumber);
                }
                chatSendLocal(`You have successfully ${actionStr} <!${target.Name}!> to use the following commands: ${commands.join(", ")}`);
            }
        }
        this.report = {
            name: "report",
            description: "Report bug",
            args: "<![text]!>",
            danger: 0,
            action: async (text) => {
                if (!text) return chatSendLocal(`Example: /bcc ${this.report.name} ${this.report.args}`);
                const data = {
                    embeds: [
                        {
                            title: "🚨 Сообщение об ошибке",
                            description: text,
                            color: 16711680,
                            footer: {
                                text: `ID: ${Player.MemberNumber}, Комната: ${ChatRoomLastName}`
                            },
                            author: {
                                name: Player.Nickname ? `${Player.Name} (${Player.Nickname})` : Player.Name
                            }
                        }
                    ]        
                }
                const res = await fetch(reportWebhookURL, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                if (!res.ok) return chatSendLocal(`Example: /bcc ${this.report.name} ${this.report.args}`);
                chatSendLocal("The report message was successfully sent to the developer!");
            }
        }
        this.help = {
            name: "help",
            description: "Open Bondage Club Chaos help menu",
            args: "",
            danger: 0,
            action: (text) => {
                let message = "<p style='padding-top:16.272px; text-align: center; color: yellow;'>۞ Bondage Club Chaos ۞</p>";
                message += `<p style='padding: 5px; text-align: center;'>Commands marked with yellow are recommended to be used with caution, commands marked with red are not recommended to be used (Can be used only in extreme cases). This addon is private, don't share it with anyone. Please do not abuse the dangerous features of this addon.</p><p style='padding: 5px;'><![filter]!> is a digit 0 or 1.<br>0 - only clothes will be involved in the command.<br>1 - clothing and restraints will be involved in the command.</p><p style='padding: 5px;'>Developer's discord: <!raidendeo!><br>BCC version: <!${version}!></p>`;
                message += `<div style="display: flex; justify-content: center;"><button style="cursor: pointer; box-shadow: 0px 0px 20px 0px black; padding: 5px 10px;font-size: 1.5vw; background: black; color: white; border: none;border-radius: 5px; animation: pulse 2.5s ease-in-out infinite;" class="bccMagicBtn" onclick="window.bccBtnCallbacks.sendDarkMagicTutorial();">🪄 Dark Magic</button></div><br>`;

                Object.keys(this).forEach((command) => {
                    let color;

                    switch (this[command].danger) {
                        case 0:
                            color = "lightgreen";
                            break;
                        case 1:
                            color = "yellow";
                            break;
                        case 2:
                            color = "red";
                            break;
                    }
                    if (this[command].args) {
                        message += `<div style='padding:5px; margin-top:4px; background:#7d39bd;'><b style='color: ${color};'>/bcc ${this[command].name}</b> ${this[command].args} - ${this[command].description}</div>`;
                    } else {
                        message += `<div style='padding:5px; margin-top:4px; background:#7d39bd;'><b style='color: ${color};'>/bcc ${this[command].name}</b> - ${this[command].description}</div>`;
                    }
                });
                chatSendLocal(message, "left");
            }
        }
    }
}

function run() {
    CommandCombine([
        {
            Tag: "bcc",
            Description: "Execute BCC command",
            Action: function (text) {
                const command = text.split(" ")[0];
                const commandText = text.split(" ").slice(1).join(" ");

                if (chatCommands[command]) {
                    chatCommands[command].action(commandText);
                } else {
                    chatSendLocal("Unknown command, use <!/bcc help!> to view a list of all available commands!");
                }
            }
        }
    ]);

    const bccStyles = document.createElement("style");
    bccStyles.innerText = `
    @keyframes pulse {
        0% {
            transform: scale(1);
        }
        50% {
            transform: scale(1.1);
        }
        100% {
            transform: scale(1);
        }
    }
    .bccMagicBtn:hover {color: black !important; background: white !important; box-shadow: 0px 0px 20px 0px white !important;}
    `;
    document.body.appendChild(bccStyles);

    bccStorage = Object.assign({}, getBCCStorage());
    setInterval(updateBCCStorage, 1000);
    registerSockets();
    registerHooks();
    registerActivities();

    function attempt() {
        const target = Player;
        const item = InventoryGet(target, target.FocusGroup.Name);

        if (target.MemberNumber === Player.MemberNumber && target.FocusGroup) {
            const itemName = item.Craft ? item.Craft.Name : item.Asset.Description;
            if (
                bccStorage.abdl.role === "baby" &&
                item?.Asset?.Category?.includes("ABDL")
            ) {
                notify("You are forbidden to remove ABDL items, baby", 3000);
                chatSendCustomAction(`Baby ${Player.Name} tried to remove ${itemName} without mommy's permission`);
                return;
            }
            if (bccStorage.darkMagic.helpless) {
                notify("You're too helpless to try to do anything");
                chatSendCustomAction(`${Player.Name} tried to remove ${itemName} but she is too helpless to do it`);
                return;
            }
            if (Player.IsRestrained() && playerHasMilkBottle()) {
                notify("You can't spit out a bottle of milk", 3000);
                chatSendCustomAction(`${Player.Name} tried to spit out a bottle of milk`);
            }
        }
    }

    registerButton("BCC_Remove", "Blocked by BCC", `${staticPath}/images/remove.png`, attempt);
    registerButton("BCC_Escape", "Blocked by BCC", `${staticPath}/images/escape.png`, attempt);
    registerButton("BCC_Struggle", "Blocked by BCC", `${staticPath}/images/struggle.png`, attempt);
    registerButton("BCC_Dismount", "Blocked by BCC", `${staticPath}/images/dismount.png`, attempt);

    if (bccStorage.skyShield.state) {
        turnOnSkyShield();
    }
    if (bccStorage.permBonus) {
        const id = setInterval(() => {
            if (!bccStorage.permBonus) {
                return clearInterval(id);
            }
            let skills = Player.Skill;
            skills.forEach((skill) => {
                skill.ModifierLevel = 5;
                skill.ModifierTimeout = Date.now() + 3600000;
            });
            ServerSend("AccountUpdate", {
                Skill: skills
            });
        }, 100000);
    }

    if (bccStorage.version !== version) {
        if (ServerPlayerIsInChatRoom()) {
            bccStorage.version = version;
            chatSendLocal("The BCC addon has been updated, check out the changes using the command <!/bcc help!>!");
        } else {
            ServerSocket.once("ChatRoomSync", () => {
                bccStorage.version = version;
                chatSendLocal("The BCC addon has been updated, check out the changes using the command <!/bcc help!>!");
            });
        }
    }

    if (!bccStorage.abdl.requests) {
        bccStorage.abdl.requests = [];
    }
    if (!bccStorage.skyShield.health) {
        bccStorage.skyShield.health = 100;
    }

    if (ServerPlayerIsInChatRoom()) {
        chatSendBCCMessage("Hello");
    }
    //Fixing a bug with BCC recognition
    setInterval(function () {
        if (ServerPlayerIsInChatRoom()) {
            chatSendBCCMessage("Hello");
        }
    }, 10000);

    if (bccStorage.asleep) {
        const a = setInterval(function () {
            if (!bccStorage.asleep) {
                CharacterSetFacialExpression(Player, "Eyes", null);
                CharacterSetFacialExpression(Player, "Emoticon", null);
                ChatRoomCharacterUpdate(Player);
                return clearInterval(a);
            }
            CharacterSetFacialExpression(Player, "Eyes", "Closed");
            CharacterSetFacialExpression(Player, "Emoticon", "Sleep");
            ChatRoomCharacterUpdate(Player);
        }, 5000);
    }

    if (bccStorage.darkMagic.licking) {
        const a = setInterval(() => {
            if (!bccStorage.darkMagic.licking) {
                return clearInterval(a);
            }
            const target = getPlayer(bccStorage.darkMagic.licking);
            if (target) forceLickAction(target);
        }, 60000);
    }

    milkGiven = playerHasMilkBottle();
    console.log("BCC: Addon was successfully launched!");
    notify("BCC was successfully launched (/bcc help)", 4000);
}

const chatCommands = new ChatCommands();
let lastCustomAction = Date.now();
let milkGiven = false;

if (window.BCC_LOADED) {
    notify("BCC is already loaded", 3000);
} else {
    window.BCC_LOADED = true;
    var bccStorage;
    console.log("BCC: Addon was successfully loaded!");
    if (Player.MemberNumber) run();
    else {
        ServerSocket.once("LoginResponse", () => {
            run();
        });
    }
}


 


